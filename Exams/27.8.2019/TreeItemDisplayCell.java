import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class TreeItemDisplayCell extends TreeCell<Task> {
    // TextField that serves as an editor
    private TextField textTF;


    // method switching cell into the editation state
    public void startEdit() {
        super.startEdit();

        // creates editor if it is not already available
        if (textTF == null) {
            createEditor();
        }

        // disabling renderer
        setText(null);
        // setting up editor - creatign content
        textTF.setText(createEditorContent());
        // displaying editor in the cell
        setGraphic(textTF);
    }

    // method switching cell into displaying state
    public void cancelEdit() {
        super.cancelEdit();

        // setting content of the renderer (label)
        setText(getFormattedContent());
        // removing editor from the cell
        setGraphic(null);
    }

    // method setting updated value to the cell
    public void updateItem(Task item, boolean empty) {
        super.updateItem(item, empty);

        // nothing is displayed for empty cell
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            // in editation state
            if (isEditing()) {
                if (textTF != null) {
                    // setting content of the editor
//                    textTF.setText(createEditorContent());
                    textTF.setText(createEditorContent());
                    // disabling renderer
                    setText(null);
                    // adding editor to the scene graph
                    setGraphic(textTF);
                }
                // in displaying state
            } else {
                // creating content of the renderer
                setText(getFormattedContent());
                // disabling editor
                setGraphic(null);
            }
        }
    }

    private String getFormattedContent() {
        if (getTreeItem().isLeaf()) {
            setFont(Font.font("sansserif", FontPosture.ITALIC, 12));
        }
        else if (getTreeItem().isExpanded()) {
            setFont(Font.font("sansserif", FontWeight.BOLD, 12));
        }
        else if (!getTreeItem().isExpanded()) {
            setFont(Font.font("sansserif", FontPosture.REGULAR, 12));
        }
        return formatContentPrefix() + formatContentSuffix();
    }

    private String formatContentPrefix() {
        return getFoldersSymbol() + getItem().getTaskType().toString() + ": ";
    }

    private String formatContentSuffix() {
        return getItem().getDescription() + getFormattedDescription();
    }

    private String getFormattedDescription() {
        if (getTreeItem().isLeaf() && !getTreeItem().getValue().getTaskType().equals(Task_Type.DATABASE)) {
            return " (Pri.: " + getTreeItem().getValue().getTaskPriority().toString() +
                    ", Odh.: " + getTreeItem().getValue().getEstTime().toString() +
                    ", Deadl.: " + getTreeItem().getValue().getDateTo().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")) +
                    ", Zbylo: " + getTreeItem().getValue().getTimeLeft().toString() + ")";
        }

        return "";
    }

    private String getFoldersSymbol() {
        if (getTreeItem().isLeaf())
            return "■ ";
        else if (!getTreeItem().isExpanded())
            return "\uD83D\uDCC1 ";
        else if (getTreeItem().isExpanded())
            return "\uD83D\uDCC2 ";
        return "■ ";
    }

    // creates content for the editor
    private String createEditorContent() {
        // editor displays only name - gender symbol is not part of the name that
        // is changed in editor
        return getItem().getDescription();
    }

    // creates editor itself
    private void createEditor() {
        // editor is based on the text field
        textTF = new TextField();
        // adding reaction to the pressed key, in order to determine if the
        // new value was commited or if the editation was canceled
        textTF.setOnKeyReleased(event -> {
            // when editation is confirmed
            if (event.getCode() == KeyCode.ENTER) {
                // veryfying if new value was really provided
                if (textTF.getText().length() == 0) {
                    // when no value was provided, editation is canceled
                    cancelEdit();
                    // when new value was provided and confirmed
                } else {
                    // getting acces to the element from the model, representing
                    // edited value
                    Task organism = getItem();
                    // new name is set to the model
                    organism.setDescription(textTF.getText());
                    // commit event is fired
                    commitEdit(organism);
                }
                // when editation is canceled
            } else if (event.getCode() == KeyCode.ESCAPE) {
                // cancel event is fired
                cancelEdit();
            }
        });
    }
}
