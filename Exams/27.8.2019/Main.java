import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

public class Main extends Application {
    public static final String DEF_LANGUAGE = "cs";
    public static final String DEF_COUNTRY = "CZ";

    private final int WINDOW_WIDTH = 860;
    private final int WINDOW_HEIGHT = 600;

    private Service service;
    private TreeView<Task> task_TV;
    private Label path_LB;
    private Label timeTotal_LB;
    private TextField estTime_TF;
    private TextField dateTo_TF;
    private TextField desc_TF;
    private TextField timeLeft_TF;
    private ChoiceBox<String> priority_CB;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        service = new Service(); //Create methods service instance.
        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));
        primaryStage.setTitle("Tasks database demonstration");
        primaryStage.setScene(createScene());
        primaryStage.setMinWidth(712);
        primaryStage.setMinHeight(384);
        primaryStage.setOnCloseRequest(event -> confirmExit());

        primaryStage.show();
    }

    private Scene createScene() {
        return new Scene(getRoot(), WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();

        rootPane.setCenter(getTree());
        rootPane.setBottom(getControlPane());
        //rootPane.setRight(getDetailsPane());
        rootPane.setTop(getMenuPane());

        return rootPane;
    }

    private Node getTree() {
        task_TV = new TreeView<>();
        task_TV.setCellFactory(treeView -> new TreeItemDisplayCell());
        task_TV.setEditable(true);
        task_TV.getSelectionModel().selectedItemProperty().addListener((javafx.beans.value.ChangeListener<? super TreeItem<Task>>) (observable, oldValue, newValue) -> {
            timeLeft_TF.setText(observable.getValue().getValue().getTimeLeft().toString());
            timeTotal_LB.setText("Čas všech úkolů: 00:00");
            estTime_TF.setText(observable.getValue().getValue().getEstTime().toString());
            dateTo_TF.setText(observable.getValue().getValue().getDateTo().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            desc_TF.setText(observable.getValue().getValue().getDescription());
            priority_CB.setValue(observable.getValue().getValue().getTaskPriority().toString());
        });
        task_TV.setOnEditCommit(event -> service.sortChildren(event.getTreeItem().getParent()));


        BorderPane.setMargin(task_TV, new Insets(5));
        task_TV.setRoot(new TreeItem<>(new Task("Seznam všech úkolů", Task_Type.DATABASE)));
        service.createDefaultTasks(task_TV.getRoot());
        task_TV.getRoot().setExpanded(true);
        task_TV.setMinWidth(200);
        task_TV.setMinHeight(150);
        return task_TV;
    }

    private Node getControlPane() {
        VBox result = new VBox(5);
        result.setPadding(new Insets(5));

        HBox dateToUpdCont = new HBox(5);
        HBox estTimeUpdCont = new HBox(5);

        HBox pathAndTotalTime_Cont = new HBox(5);
        pathAndTotalTime_Cont.setPadding(new Insets(5));

        //HBox path_hbox = new HBox();
        HBox totalTime_hbox = new HBox();

        timeTotal_LB = new Label("Čas všech úkolů: 00:00 hod.");

        HBox inputsContainer = new HBox(5);
        inputsContainer.setPadding(new Insets(5));

        VBox inputCont_1 = new VBox(5),
                inputCont_2 = new VBox(5),
                inputCont_3 = new VBox(5);

        Label estTime_LB = new Label("Odhad. čas:"),
                dateTo_LB = new Label("Datum do:"),
                priority_LB = new Label("Priorita:"),
                desc_LB = new Label("Popisek:");

        Button updateEstTime = new Button("Update");
        updateEstTime.setOnAction(event -> {
            LocalTime current = task_TV.getSelectionModel().getSelectedItem().getValue().getEstTime();
            try {
                task_TV.getSelectionModel().getSelectedItem().getValue().setEstTime(LocalTime.parse(estTime_TF.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                if (task_TV.getSelectionModel().getSelectedItem().getValue().getEstTime().getHour() > 8) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Chyba aktualizace");
                    alert.setHeaderText("Zadaný čas je větší než 8 hodin");
                    alert.setContentText("Zadejte prosím čas ve správném formatu HH:MM ménší než 8 hodin.");
                    task_TV.getSelectionModel().getSelectedItem().getValue().setEstTime(current);
                    alert.showAndWait();
                }
                task_TV.refresh();
            } catch (DateTimeParseException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba aktualizace");
                alert.setHeaderText("Zadaný čas je spatný");
                alert.setContentText("Zadejte prosím čas ve správném formatu.");
                alert.showAndWait();
            }
        });
        estTime_TF = new TextField();
        estTimeUpdCont.getChildren().addAll(estTime_TF, updateEstTime);

        Button updateDate = new Button("Update");
        updateDate.setOnAction(event -> {
            try {
                task_TV.getSelectionModel().getSelectedItem().getValue().setDateTo(LocalDateTime.parse(dateTo_TF.getText(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                task_TV.refresh();
            } catch (DateTimeParseException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba aktualizace");
                alert.setHeaderText("Zadaný datum je spatný");
                alert.setContentText("Zadejte prosím datum ve správném formatu.");
                alert.showAndWait();
            }
        });
        dateTo_TF = new TextField();
        dateToUpdCont.getChildren().addAll(dateTo_TF, updateDate);

        desc_TF = new TextField();
        desc_TF.textProperty().addListener((observable, oldValue, newValue) -> {
            task_TV.getSelectionModel().getSelectedItem().getValue().setDescription(desc_TF.getText());
            task_TV.refresh();
        });

        timeLeft_TF = new TextField();
        priority_CB = new ChoiceBox<>();

        for (Task_Priority value : Task_Priority.values()) {
            priority_CB.getItems().add(value.toString());
        }
        priority_CB.setValue(priority_CB.getItems().get(0));

        priority_CB.valueProperty().addListener((observable, oldValue, newValue) ->{

            task_TV.getSelectionModel().getSelectedItem().getValue().setTaskPriority(Task_Priority.LOW);
            task_TV.refresh();
        });

        HBox buttonsContainer = new HBox(10);
        buttonsContainer.setAlignment(Pos.CENTER);

        Button expandAllTree = new Button("_Expand All");
        expandAllTree.setOnAction(event -> service.expandAllNodes(task_TV.getRoot()));

        Button collapseAllTree = new Button("_Collapse All");
        collapseAllTree.setOnAction(event -> service.collapseAllNodes(task_TV));

        Button addNode = new Button("_Add");
        addNode.setOnAction(event -> {
            service.addNode(task_TV.getSelectionModel().getSelectedItem(), desc_TF, desc_TF);
        });
        Button deleteNode = new Button("_Delete");
        deleteNode.setOnAction(event -> {
            service.deleteNode(task_TV.getSelectionModel().getSelectedItem());
            task_TV.getSelectionModel().clearSelection();
        });

        Button exitApp = new Button("_Ukončit Aplikaci");
        exitApp.setOnAction(event -> confirmExit());

        //Composition
        buttonsContainer.getChildren().addAll(addNode, deleteNode, expandAllTree, collapseAllTree, exitApp);
        inputCont_3.getChildren().addAll(desc_LB, desc_TF, buttonsContainer);
        inputCont_2.getChildren().addAll(dateTo_LB, dateToUpdCont, new Label("Zbylo času:"), timeLeft_TF);
        inputCont_1.getChildren().addAll(estTime_LB, estTimeUpdCont, priority_LB, priority_CB);

        inputCont_3.setAlignment(Pos.CENTER);

        inputsContainer.getChildren().addAll(inputCont_1, inputCont_2, inputCont_3);

        //path_hbox.getChildren().add(path_LB);
        totalTime_hbox.getChildren().add(timeTotal_LB);
        //path_hbox.setAlignment(Pos.CENTER_LEFT);
        totalTime_hbox.setAlignment(Pos.CENTER_RIGHT);
        pathAndTotalTime_Cont.getChildren().add(totalTime_hbox);

        BorderPane pane = new BorderPane();
        pane.setTop(pathAndTotalTime_Cont);
        pane.setCenter(inputsContainer);

        result.setAlignment(Pos.CENTER);
        result.getChildren().add(pane);
        return result;
    }

    private Node getMenuPane() {
        MenuBar menuBar = new MenuBar();
        Menu file = new Menu("_File");
        MenuItem exit = new MenuItem("_Exit");
        exit.setOnAction(event -> confirmExit());

        file.getItems().add(exit);
        menuBar.getMenus().add(file);
        return menuBar;
    }

    private void confirmExit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exiting");
        alert.setHeaderText("Do you really want to close the application?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    Platform.exit();
                });
    }

}
