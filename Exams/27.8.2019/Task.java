import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Task implements Comparable<Task>{
    private String description = "";
    private Task_Type taskType = null;
    private Task_Priority taskPriority = null;
    private LocalTime estTime = null;
    private LocalDateTime dateTo = null;

    public Task(String description, Task_Type taskType, Task_Priority taskPriority, LocalTime estTime, LocalDateTime dateTo) {
        this.description = description;
        this.taskType = taskType;
        this.taskPriority = taskPriority;
        this.estTime = estTime;
        this.dateTo = dateTo;
        initNullVariables();
    }

    public Task(String description, Task_Type taskType, Task_Priority taskPriority) {
        this.description = description;
        this.taskType = taskType;
        this.taskPriority = taskPriority;

        initNullVariables();
    }

    public Task(String description, Task_Type taskType) {
        this.description = description;
        this.taskType = taskType;

        initNullVariables();
    }

    public Task_Type getTaskType() {
        return taskType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task_Priority getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Task_Priority taskPriority) {
        this.taskPriority = taskPriority;
    }

    private void initNullVariables() {
        if (description == null)
            description = "";
        if (taskPriority == null)
            taskPriority = Task_Priority.LOW;
        if (taskType == null)
            taskType = Task_Type.TASK;
        if (estTime == null)
            estTime = LocalTime.of(0, 0);
        if (dateTo == null)
            dateTo = LocalDateTime.now();
    }

    public LocalTime getEstTime() {
        return estTime;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public LocalTime getTimeLeft() {
        //return LocalTime.of((this.dateTo.getHour() - LocalTime.now().getHour()), (this.dateTo.getMinute() - LocalTime.now().getMinute()));
        return LocalTime.of(0, 0);
    }

    @Override
    public int compareTo(Task o) {
        return this.getDescription().compareTo(o.getDescription());
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }

    public void setEstTime(LocalTime estTime) {
        this.estTime = estTime;
    }

    public void setTaskType(Task_Type taskType) {
        this.taskType = taskType;
    }
}
