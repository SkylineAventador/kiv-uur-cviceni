public enum Task_Priority {
    LOW, MEDIUM, HIGH;
//    public static Task_Priority[] valuesPool = values();
    @Override
    public String toString() {
        switch (this){
            case LOW:
                return "Nízká";
            case MEDIUM:
                return "Střední";
            case HIGH:
                return "Vysoká";
        }
        return "Žádná";
    }

//    public
}
