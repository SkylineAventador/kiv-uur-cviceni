public enum Task_Type {
    DATABASE, LIST, TASK;
    private static Task_Type[] valuesPool = values();
    @Override
    public String toString() {
        switch (this){
            case DATABASE:
                return "Databáze";
            case LIST:
                return "Seznam";
            case TASK:
                return "Úkol";
        }
        return "Neznámo";
    }

    public Task_Type getNext(){
        if (this == LIST)
            return TASK;
        else return LIST;
    }
}
