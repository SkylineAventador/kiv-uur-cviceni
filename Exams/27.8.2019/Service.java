import javafx.scene.control.*;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Locale;

public class Service {
    public void sortChildren(TreeItem<Task> parent) {
        if (parent != null) {
           Collator coll = Collator.getInstance(Locale.getDefault());
           coll.setStrength(Collator.PRIMARY);
            parent.getChildren().sort((o1, o2) -> coll.compare(o1.getValue().getDescription(), o2.getValue().getDescription()));
            parent.getChildren().sort(Comparator.comparing(item -> item.getValue().getDescription()));
        }
    }

    public void createDefaultTasks(TreeItem<Task> parent) {
        TreeItem<Task> seznam_1 = new TreeItem<>(new Task("Seznam úkolů č.1", Task_Type.LIST));
        TreeItem<Task> seznam_2 = new TreeItem<>(new Task("Seznam úkolů č.2", Task_Type.LIST));

        TreeItem<Task> ukol_1_1 = new TreeItem<>(new Task("Absolvovat zkoušku na poprvé", Task_Type.TASK, Task_Priority.LOW));
        TreeItem<Task> ukol_1_2 = new TreeItem<>(new Task("Absolvovat zkoušku po druhé", Task_Type.TASK, Task_Priority.MEDIUM));
        TreeItem<Task> ukol_1_3 = new TreeItem<>(new Task("Absolvovat zkoušku po třetí", Task_Type.TASK, Task_Priority.HIGH));

        TreeItem<Task> ukol_2_1 = new TreeItem<>(new Task("Donest si index zkoušejícímu na poprvé",  Task_Type.TASK, Task_Priority.LOW));
        TreeItem<Task> ukol_2_2 = new TreeItem<>(new Task("Donest si index zkoušejícímu po druhé", Task_Type.TASK, Task_Priority.MEDIUM));
        TreeItem<Task> ukol_2_3 = new TreeItem<>(new Task("Donest si index zkoušejícímu po třetí", Task_Type.TASK, Task_Priority.HIGH));

        seznam_1.getChildren().addAll(ukol_1_1, ukol_1_2, ukol_1_3);
        seznam_2.getChildren().addAll(ukol_2_1, ukol_2_2, ukol_2_3);
        parent.getChildren().addAll(seznam_1, seznam_2);
    }

    public void expandAllNodes(TreeItem<Task> node) {
        if (!node.isLeaf()) {
            node.setExpanded(true);
            node.getChildren().forEach(this::expandAllNodes);
        }
    }

//    public LocalTime countAllTasksTime(TreeItem<Task> node) {
//        if (node.getChildren() != null) {
//            totalTime += node.getValue().getEstTime();
//            node.getChildren().forEach(this::expandAllNodes);
//        }
//        return new totalTime;
//    }

    public void collapseAllNodes(TreeView<Task> treeView) {
        treeView.getRoot().getChildren().forEach(this::collapseChildrenNodes);
        treeView.getRoot().setExpanded(true);
    }

    private void collapseChildrenNodes(TreeItem<Task> node) {
        if (node.getParent() != null) {
            node.setExpanded(false);
            node.getChildren().forEach(this::collapseChildrenNodes);
        }
    }

    public void addNode(TreeItem<Task> selectedNode, TextField nameNode, TextField descNode) {
        if (selectedNode == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Chyba přidání");
            alert.setHeaderText("Nic není vybráno!");
            alert.setContentText("Vyberte prosím jiný úzel pro přidání úkolu.");
            alert.showAndWait();
        } else {
            if (nameNode.getText().length() == 0) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba přidání");
                alert.setHeaderText("Není zadán popisek úkolu");
                alert.setContentText("Zadejte prosím co je potřeba udělat.");
                alert.showAndWait();
            }  else {
                Task newTask;
                newTask = new Task(nameNode.getText(), selectedNode.getValue().getTaskType().equals(Task_Type.LIST) ? selectedNode.getValue().getTaskType().getNext() : Task_Type.TASK);
                selectedNode.getChildren().add(new TreeItem<Task>(newTask));
                sortChildren(selectedNode);
                if (selectedNode.getValue().getTaskType() != Task_Type.LIST) {
                    selectedNode.getValue().setTaskType(selectedNode.getValue().getTaskType().getNext());
                }
                selectedNode.setExpanded(true);
            }
        }
    }

    public void deleteNode(TreeItem<Task> selectedNode) {
        // if nothing is selected, warning is displayed
        if (selectedNode == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Chyba mazání");
            alert.setHeaderText("Není nic co by šlo zmazat");
            alert.setContentText("Vyberte prosím jiný úzel pro mazání.");
            alert.showAndWait();
        } else {
            // obtaining parent of selected item
            TreeItem<Task> parent = selectedNode.getParent();

            // root cannot be deleted - if parent is null, nothing will be deleted
            if (parent == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba mazání");
                alert.setHeaderText("Vybraný úzel je databáze!");
                alert.setContentText("Není možné smazat databázi.");
                alert.showAndWait();
                // when selected element is not root
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Mazání");
                alert.setHeaderText("Opravdu chcete smazat seznam/úkol a všechny podseznamy/podúkoly?");
                alert.setContentText("Vybráné: " + selectedNode.getValue().getTaskType().toString() + " -> " + selectedNode.getValue().getDescription());
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK)
                        .ifPresent(response -> {
                            // removing selected element from its parent
                            parent.getChildren().remove(selectedNode);
                        });
            }
        }
    }
}
