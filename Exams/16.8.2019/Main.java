import com.sun.prism.paint.Paint;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.event.ChangeListener;
import java.beans.EventHandler;
import java.util.Locale;

public class Main extends Application {
    public static final String DEF_LANGUAGE = "cs";
    public static final String DEF_COUNTRY = "CZ";

    private final int WINDOW_WIDTH = 800;
    private final int WINDOW_HEIGHT = 600;

    private Service service;
    private TreeView<Organism> organism_TV;
    private Label path_LB;
    private TextField name_TF;
    private TextField desc_TF;
    private Label details_LB;
    private TextArea details_TA;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        service = new Service(); //Create methods service instance.
        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));
        primaryStage.setTitle("Organisms tree demonstration");
        primaryStage.setScene(createScene());
        // TODO: 16.08.2019 Change may be later minimal resolution
        primaryStage.setMinWidth(712);
        primaryStage.setMinHeight(384);

        primaryStage.show();
    }

    private Scene createScene() {
        return new Scene(getRoot(), WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();

        rootPane.setCenter(getTree());
        rootPane.setBottom(getControlPane());
        rootPane.setRight(getDetailsPane());
        rootPane.setTop(getMenuPane());

        return rootPane;
    }

    private Node getTree() {
        organism_TV = new TreeView<>();
        organism_TV.setCellFactory(treeView -> new TreeItemDisplayCell());
        organism_TV.setEditable(true);
        organism_TV.getSelectionModel().selectedItemProperty().addListener((javafx.beans.value.ChangeListener<? super TreeItem<Organism>>) (observable, oldValue, newValue) -> {
            path_LB.setText("Node path: " + service.buildNodePath(organism_TV));
            name_TF.setPromptText("Enter organism's name here. Adding to: " + organism_TV.getSelectionModel().getSelectedItem().getValue().getClassification().toString());
            updateDetailsPane(organism_TV.getSelectionModel().getSelectedItem());
        });
        organism_TV.setOnEditCommit(event -> service.sortChildren(event.getTreeItem().getParent()));

        BorderPane.setMargin(organism_TV, new Insets(5));
        organism_TV.setRoot(new TreeItem<>(new Organism(Orgamism_Type.ZIVOT)));
        service.createDefaultOrganisms(organism_TV.getRoot());
        organism_TV.setMinWidth(200);
        organism_TV.setMinHeight(150);
        return organism_TV;
    }

    private Node getControlPane() {
        // TODO: 16.08.2019 + 2 ply Vbox and the path label at the top.
        VBox result = new VBox(5);
        result.setPadding(new Insets(5));
        HBox buttonsContainer = new HBox(10);
        buttonsContainer.setAlignment(Pos.CENTER);
        path_LB = new Label("Node path: Empty");


        VBox inputsContainer = new VBox(10);
        inputsContainer.getChildren().addAll(name_TF = new TextField(), desc_TF = new TextField());
        name_TF.setPromptText("Enter organism's name here.");
        desc_TF.setPromptText("Enter organism's description here...");

        Button expandAllTree = new Button("_Expand All");
        expandAllTree.setOnAction(event -> service.expandAllNodes(organism_TV.getRoot()));

        Button collapseAllTree = new Button("_Collapse All");
        collapseAllTree.setOnAction(event -> service.collapseAllNodes(organism_TV));

        Button addNode = new Button("_Add");
        addNode.setOnAction(event -> {
            service.addNode(organism_TV.getSelectionModel().getSelectedItem(), name_TF, desc_TF);
        });
        Button deleteNode = new Button("_Delete");
        deleteNode.setOnAction(event -> {
            service.deleteNode(organism_TV.getSelectionModel().getSelectedItem());
            organism_TV.getSelectionModel().clearSelection();
        });

        buttonsContainer.getChildren().addAll(addNode, deleteNode, expandAllTree, collapseAllTree);
        result.getChildren().addAll(path_LB, inputsContainer, buttonsContainer);
        return result;
    }

    private Node getDetailsPane() {
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(5));
        details_LB = new Label("Nothing is selected");
        details_TA = new TextArea("Select a leaf to view and change its description.");
        details_TA.setWrapText(true);
        details_TA.setDisable(true);
        details_TA.setPrefColumnCount(15);
        details_TA.textProperty().addListener((observable, oldValue, newValue) -> {
            updateNodeDescription(organism_TV.getSelectionModel().getSelectedItem());
        });
        borderPane.setCenter(details_TA);
        borderPane.setBottom(details_LB);

        return borderPane;
    }

    private Node getMenuPane() {
        MenuBar menuBar = new MenuBar();
        Menu file = new Menu("_File");
        MenuItem exit = new MenuItem("_Exit");
        exit.setOnAction(event -> confirmExit());

        file.getItems().add(exit);
        menuBar.getMenus().add(file);
        return menuBar;
    }

    private void confirmExit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exiting");
        alert.setHeaderText("Do you really want to close the application?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    Platform.exit();
                });
    }

    private void updateDetailsPane(TreeItem<Organism> selected) {
        if (selected.getValue().getClassification() == Orgamism_Type.DRUH) {
            details_LB.setText("Above you can view and change description of the organism: " + selected.getValue().getName());
            details_TA.setText(selected.getValue().getDescription());
            details_TA.setDisable(false);
        } else {
            details_LB.setText("Nothing is selected");
            details_TA.setText("Select a leaf to view and change its description.");
            details_TA.setDisable(true);
        }
    }

    private void updateNodeDescription(TreeItem<Organism> selected) {
        selected.getValue().setDescription(details_TA.getText());
        organism_TV.refresh();
    }

}
