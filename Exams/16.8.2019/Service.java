import com.sun.org.apache.xpath.internal.operations.Or;
import javafx.scene.Node;
import javafx.scene.control.*;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static com.sun.org.apache.xml.internal.security.keys.keyresolver.KeyResolver.length;

public class Service {
    public void createDefaultOrganisms(TreeItem<Organism> parent) {
        TreeItem<Organism> rise = new TreeItem<>(new Organism("živočichové", Orgamism_Type.RISE));

        TreeItem<Organism> kmen = new TreeItem<>(new Organism("strunatci", Orgamism_Type.KMEN));

        TreeItem<Organism> trida = new TreeItem<>(new Organism("savci", Orgamism_Type.TRIDA));

        TreeItem<Organism> rad_1 = new TreeItem<>(new Organism("hlodavci", Orgamism_Type.RAD));
        TreeItem<Organism> rad_2 = new TreeItem<>(new Organism("šelmy", Orgamism_Type.RAD));

        TreeItem<Organism> celed_1 = new TreeItem<>(new Organism("veverkovití", Orgamism_Type.CELED));
        TreeItem<Organism> celed_2 = new TreeItem<>(new Organism("psovití", Orgamism_Type.CELED));

        TreeItem<Organism> rod_1 = new TreeItem<>(new Organism("veverka", Orgamism_Type.ROD));
        TreeItem<Organism> rod_2 = new TreeItem<>(new Organism("pes", Orgamism_Type.ROD));
        TreeItem<Organism> rod_3 = new TreeItem<>(new Organism("liška", Orgamism_Type.ROD));

        TreeItem<Organism> druh_1 = new TreeItem<>(new Organism("deppeho", Orgamism_Type.DRUH));
        TreeItem<Organism> druh_2 = new TreeItem<>(new Organism("červená / obecná", Orgamism_Type.DRUH));
        TreeItem<Organism> druh_3 = new TreeItem<>(new Organism("vlk", Orgamism_Type.DRUH, "Canis lupis"));
        TreeItem<Organism> druh_4 = new TreeItem<>(new Organism("kojot", Orgamism_Type.DRUH, "Canis latrans"));
        TreeItem<Organism> druh_5 = new TreeItem<>(new Organism("horská", Orgamism_Type.DRUH));
        TreeItem<Organism> druh_6 = new TreeItem<>(new Organism("pisečná", Orgamism_Type.DRUH));
        TreeItem<Organism> druh_7 = new TreeItem<>(new Organism("pouštní", Orgamism_Type.DRUH));

        rod_1.getChildren().addAll(druh_1, druh_2);
        rod_2.getChildren().addAll(druh_3, druh_4);
        rod_3.getChildren().addAll(druh_5, druh_6, druh_7);

        celed_1.getChildren().add(rod_1);
        celed_2.getChildren().addAll(rod_2, rod_3);

        rad_1.getChildren().add(celed_1);
        rad_2.getChildren().add(celed_2);

        trida.getChildren().addAll(rad_1, rad_2);
        kmen.getChildren().add(trida);
        rise.getChildren().add(kmen);

        parent.getChildren().add(rise);
    }

    public void expandAllNodes(TreeItem<Organism> node) {
        if (!node.isLeaf()) {
            node.setExpanded(true);
            node.getChildren().forEach(this::expandAllNodes);
        }
    }

    public void collapseAllNodes(TreeView<Organism> treeView) {
        treeView.getRoot().getChildren().forEach(this::collapseChildrenNodes);
        treeView.getRoot().setExpanded(false);
    }

    private void collapseChildrenNodes(TreeItem<Organism> node) {
        if (node.getParent() != null) {
            node.setExpanded(false);
            node.getChildren().forEach(this::collapseChildrenNodes);
        }
    }

    public void addNode(TreeItem<Organism> selectedNode, TextField nameNode, TextField descNode) {
        if (selectedNode == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Insertion error");
            alert.setHeaderText("Nothing is selected!");
            alert.setContentText("Please select node to add new organism.");
            // and alert is displayed
            alert.showAndWait();
            // if some node is selected as an ancestor
        } else {
            // checking if name is provided
            if (nameNode.getText().length() == 0) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Insertion error");
                alert.setHeaderText("No name is provided!");
                alert.setContentText("Please provide a name for a new organism.");
                // when no name is provided, adding cannot continue
                alert.showAndWait();
            } else if (selectedNode.getValue().getClassification() == Orgamism_Type.DRUH) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Insertion error");
                alert.setHeaderText("Border organism type is selected!");
                alert.setContentText("It is impossible to add to it. Please select another node.");
                // and alert is displayed
                alert.showAndWait();
            } else {
                    // new ancestor is created
                Organism newOrganism;
                if (descNode.getText().length() == 0)
                    newOrganism = new Organism(nameNode.getText(), selectedNode.getValue().getClassification().getNext());
                else
                    newOrganism = new Organism(nameNode.getText(), selectedNode.getValue().getClassification().getNext(), descNode.getText());
                    // ancestor is added as a child to selected node
                selectedNode.getChildren().add(new TreeItem<Organism>(newOrganism));
                sortChildren(selectedNode);
                // all siblings of added element are sorted
                // selected node (parent of the new Ancestor) is expanded
                // ensures that newly added node will be visible
                selectedNode.setExpanded(true);
            }
        }
    }

    public void deleteNode(TreeItem<Organism> selectedNode) {
        // if nothing is selected, warning is displayed
        if (selectedNode == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Deleting error");
            alert.setHeaderText("Nothing is selected!");
            alert.setContentText("Please select node to delete.");
            // shows warning
            alert.showAndWait();
            // when something is selected
        } else {
            // obtaining parent of selected item
            TreeItem<Organism> parent = selectedNode.getParent();

            // root cannot be deleted - if parent is null, nothing will be deleted
            if (parent == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Deleting error");
                alert.setHeaderText("Selected item is Root!");
                alert.setContentText("It is not possible to delete " + selectedNode.getValue().getName() + ".");
                alert.showAndWait();
                // when selected element is not root
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Deleting");
                alert.setHeaderText("Do you want to delete selected node and all subcategories?");
                alert.setContentText("Selected: " + selectedNode.getValue().getClassification().toString() + " -> " + selectedNode.getValue().getName());
                // asking if the selected element should be really removed, along with its offsprings
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK)
                        .ifPresent(response -> {
                            // removing selected element from its parent
                            parent.getChildren().remove(selectedNode);
                        });
            }
        }
    }

    public String buildNodePath(TreeView<Organism> treeView) {
        StringBuilder pathBuilder = new StringBuilder();
        for (TreeItem<Organism> item = treeView.getSelectionModel().getSelectedItem();
             item != null; item = item.getParent()) {

            pathBuilder.insert(0, item.getValue().getName());
            if (item != treeView.getRoot()) pathBuilder.insert(0, " -> ");
        }
        return pathBuilder.toString();
    }

    public void sortChildren(TreeItem<Organism> parent) {
        if (parent != null) {
            // TODO: 16.08.2019 Not working. Correct sorting is with Collator. Fix this for maximum points.
//            Collator coll = Collator.getInstance(Locale.getDefault());
//            coll.setStrength(Collator.PRIMARY);
//            parent.getChildren().sort(coll.compare(o1.getName(), o2.getName()));

            parent.getChildren().sort(Comparator.comparing(item -> item.getValue().getName()));
        }
    }
}
