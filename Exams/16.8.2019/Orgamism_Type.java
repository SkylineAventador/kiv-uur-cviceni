public enum Orgamism_Type {
    ZIVOT, DOMENA, RISE, KMEN, TRIDA, RAD, CELED, ROD, DRUH;

    private static Orgamism_Type[] valuesPool = values();

    @Override
    public String toString() {
        switch (this){
            case ZIVOT:{
                return "Život";
            }

            case DOMENA:{
                return "Doména";
            }

            case RISE:{
                return "Říše";
            }

            case KMEN:{
                return "Kmen";
            }

            case TRIDA:{
                return "Třída";
            }

            case RAD:{
                return "Řád";
            }

            case CELED:{
                return "Čeleď";
            }

            case ROD:{
                return "Rod";
            }

            case DRUH:{
                return "Druh";
            }
        }
        return "NEZNÁMÝ TYP";
    }

    public Orgamism_Type getNext() {
        return valuesPool[(this.ordinal() + 1) % valuesPool.length];
    }

    public Orgamism_Type getPrevious() {
        return valuesPool[(this.ordinal() - 1) % valuesPool.length];
    }

}
