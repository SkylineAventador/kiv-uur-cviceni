public class Organism implements Comparable<Organism>{
    private String name, description;
    private Orgamism_Type classification;

    public Organism(String name, Orgamism_Type classification, String description) {
        this.name = formatName(name);
        this.description = description;
        this.classification = classification;
    }

    public Organism(String name, Orgamism_Type classification) {
        this.name = formatName(name);
        this.classification = classification;
        this.description = "";
    }

    public Organism(Orgamism_Type classification) {
        this.classification = classification;
        this.name = classification.toString();
        this.description = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Orgamism_Type getClassification() {
        return classification;
    }

    public void setClassification(Orgamism_Type classification) {
        this.classification = classification;
    }

    private String formatName(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    @Override
    public int compareTo(Organism o) {
        return this.getName().compareTo(o.getName());
    }
}
