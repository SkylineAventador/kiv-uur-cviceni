import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.text.NumberFormat;
import java.util.Locale;

public class Main extends Application {
//    Language translation is available at cv 10!!! <<<<<<<<<<<<<<<<<<<<<<<<============================================



//    double amount =200.0;
//    Locale locale = new Locale("en", "US");
//    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
//    System.out.println(currencyFormatter.format(amount));


//    double amount =200.0;
//    System.out.println(NumberFormat.getCurrencyInstance(new Locale("en", "US"))
//            .format(amount));

//    double amount = 200;
//    DecimalFormat twoPlaces = new DecimalFormat("0.00");
//    System.out.println(twoPlaces.format(amount));

//    double amount = 2000000;
//    System.out.println(String.format("%,.2f", amount));

    public static final String DEF_LANGUAGE = "cs";
    public static final String DEF_COUNTRY = "CZ";


    private Stage primaryStage;
    private ListView<String> install_listLV;
    private ListView<String> available_listLV;
    private ListView<Module> availableModules;
    private TextArea modulesInfo;
    private Label totalSize = new Label("Celkova velikost: 0 kB");


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));

        this.primaryStage.setTitle("UUR_Kravtsov");

        availableModules = new ListView<>(createInitData());
        this.primaryStage.setScene(createScene());
        this.primaryStage.show();
    }

    private Scene createScene() {
        Scene scene = new Scene(getRoot(), 800, 600);
        return scene;
    }

    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();
        rootPane.setCenter(getListsPane());
        rootPane.setTop(getMenuPane());
        rootPane.setBottom(getBottomContent());
        return rootPane;
    }

    private Node getBottomContent() {
        VBox result = new VBox(10);
        result.setPadding(new Insets(5));
        result.getChildren().addAll(getModuleInfoBox(), getControlPaneee());
        return result;
    }

    private HBox getControlPaneee() {
        HBox result = new HBox();
        result.setSpacing(10);
        Label label = new Label("Zadejte do pole nize cestu k instalaci modulu:");
        TextField cesta = new TextField();
        cesta.setPromptText("Zadejte cestu");

        Button potvrdit = new Button("Potvrdit");
        result.getChildren().addAll(label, cesta, potvrdit);

        return result;
    }

    private Node getMenuPane() {
        MenuBar menuBar = new MenuBar();
        Menu file = new Menu("_File");
        MenuItem exit = new MenuItem("E_xit");
        exit.setOnAction(event -> Platform.exit());

        file.getItems().addAll(exit);
        menuBar.getMenus().add(file);

        return menuBar;
    }

    private Node getListsPane() {
        HBox result = new HBox(10);
        result.getChildren().addAll(getInstall_listLV_Box(), getMoveButtonsPane(), getAvailable_listLV_Box());

        result.setPadding(new Insets(15, 5, 10, 5));
        result.setAlignment(Pos.CENTER);
        return result;
    }

    private VBox getMoveButtonsPane() {
        VBox result = new VBox(10);
        Button leftToRight = new Button("->");
        leftToRight.setOnAction(event -> processSelection(install_listLV, available_listLV));
        Button rightToLeft = new Button("<-");
        rightToLeft.setOnAction(event -> processSelection(available_listLV, install_listLV));

        result.getChildren().addAll(leftToRight, rightToLeft);
        result.setAlignment(Pos.CENTER);
        return result;
    }

    private VBox getInstall_listLV_Box() {
        VBox result = new VBox();
        Label title = new Label("Moduly k instalaci:");
        result.getChildren().addAll(title, getInstall_listLV());
        return result;
    }

    private VBox getAvailable_listLV_Box() {
        VBox result = new VBox();
        Label title = new Label("Zbývající moduly:");
        result.getChildren().addAll(title, getAvailable_listLV());
        return result;
    }

    private VBox getModuleInfoBox() {
        VBox result = new VBox();
        result.setSpacing(5);
        Label title = new Label("Informace o vybranych modulech:");
        modulesInfo = new TextArea();
        modulesInfo.setPromptText("Tady se zobrazi informace o vybranych k instalaci modulech");
        modulesInfo.setMaxWidth(350);
        modulesInfo.setMaxHeight(150);
        modulesInfo.setEditable(false);
        result.getChildren().addAll(title, modulesInfo, totalSize);
        return result;
    }

    private ListView<String> getInstall_listLV() {
        install_listLV = new ListView<String>();

        // filing list with initial data
//        install_listLV.setItems(getDataFullInfo(availableModules));
        //install_listLV.getItems().removeAll();

        // setting maximum spreading to sides - in order to fill whole center
//        install_listLV.setPrefWidth(350);
//        install_listLV.setPrefHeight(Region.USE_COMPUTED_SIZE);
        // allowing selection of multiple items
        install_listLV.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // allowing editation of cells
        install_listLV.setEditable(false);
        // setting a cell factory that creates editable string cells, based on TextField
        // basic cells are not able to edit their content
        install_listLV.setCellFactory(TextFieldListCell.forListView());

        // setting margin around the whole list view - this will be used by layout in the parent panel
        BorderPane.setMargin(install_listLV, new Insets(5));
        install_listLV.setOnMouseClicked(event -> {
            StringBuilder builder = new StringBuilder();
            install_listLV.getSelectionModel().getSelectedItems().forEach(item ->{
                for (int i = 0; i < availableModules.getItems().size(); i++) {
                    if (availableModules.getItems().get(i).getName().equals(item)) {
                        builder.append(availableModules.getItems().get(i).toString()).append('\n');
                    }
                }
            });
//            available_listLV.getSelectionModel().getSelectedItems().forEach(item -> builder.append(item).append('\n'));
            modulesInfo.setText(builder.toString());
        });

        return install_listLV;
    }

    private ListView<String> getAvailable_listLV() {
        available_listLV = new ListView<String>();

        // filing list with initial data
        available_listLV.setItems(getDataNamesOnly(availableModules));

        // setting maximum spreading to sides - in order to fill whole center
//        available_listLV.setPrefWidth(350);
//        available_listLV.setPrefHeight(Region.USE_COMPUTED_SIZE);
        // allowing selection of multiple items
        available_listLV.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // allowing editation of cells
        available_listLV.setEditable(false);
        // setting a cell factory that creates editable string cells, based on TextField
        // basic cells are not able to edit their content
        available_listLV.setCellFactory(TextFieldListCell.forListView());

        // setting margin around the whole list view - this will be used by layout in the parent panel
        BorderPane.setMargin(available_listLV, new Insets(5));
        available_listLV.setOnMouseClicked(event -> {
            StringBuilder builder = new StringBuilder();
            available_listLV.getSelectionModel().getSelectedItems().forEach(item ->{
                for (int i = 0; i < availableModules.getItems().size(); i++) {
                    if (availableModules.getItems().get(i).getName().equals(item)) {
                        builder.append(availableModules.getItems().get(i).toString()).append('\n');
                    }
                }
            });
//            available_listLV.getSelectionModel().getSelectedItems().forEach(item -> builder.append(item).append('\n'));
            modulesInfo.setText(builder.toString());
        });
        return available_listLV;
    }

    private Node getControlPane() {
        return null;
    }

    private ObservableList<Module> createInitData() {
        ObservableList<Module> data = FXCollections.observableArrayList();

        data.add(new Module("Graph editor", "2.0.3.", 125));
        data.add(new Module("Spellchecker", "1.0.0.", 35));
        data.add(new Module("Text editor", "1.4.2.", 225));
        data.add(new Module("Contacts", "1.1.1.", 29));
        data.add(new Module("Mail sender", "2.9.1.", 58));

        return data;
    }

    private ObservableList<String> getDataNamesOnly(ListView<Module> list) {
        ObservableList<String> data = FXCollections.observableArrayList();

        for (int i = 0; i < list.getItems().size(); i++) {
            data.add(list.getItems().get(i).getName());
        }

        return data;
    }

    private ObservableList<String> getDataFullInfo(ListView<Module> list) {
        ObservableList<String> data = FXCollections.observableArrayList();


        for (int i = 0; i < list.getItems().size(); i++) {
            data.add(list.getItems().get(i).toString());
        }

        return data;
    }

    private void processSelection(ListView<String> fromList, ListView<String> toList) {
        // creating copy of selected elements - collection containing reference on all selected strings
        ObservableList<String> selection = FXCollections.observableArrayList(fromList.getSelectionModel().getSelectedItems());

        // when nothing is selected, warning is displayed
        if (selection.size() == 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Vyberete nejake radky k preneseni!");
            alert.setTitle("Chyba vyberu");
            alert.setHeaderText("Nic nebylo vybrano!");
            alert.showAndWait();
            // when at least one item is selected, all selected items are displayed in the dialog
        } else {
            toList.getItems().addAll(selection);
            fromList.getItems().removeAll(selection);
            resetSelection(toList);
            resetSelection(fromList);
            toList.refresh();
            fromList.refresh();
            modulesInfo.setText("");

            final int[] size = {0};
            install_listLV.getItems().forEach(item -> {
                for (int i = 0; i < availableModules.getItems().size(); i++) {
                    if (availableModules.getItems().get(i).getName().equals(item)) {
                        size[0] += availableModules.getItems().get(i).getSize();
                    }
                }
            });
            totalSize.setText("Celkova velikost: " + size[0] + " kB");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Uspech");
            alert.setHeaderText("Vybrane polozky byly uspesne prenesene");
            alert.showAndWait();
        }
    }

    private void resetSelection(ListView<String> list) {
        // removing selection from list
        list.getSelectionModel().clearSelection();
    }
}
