public class Module {
    private String name, version;
    private int size; // v kilobytech

    public Module(String name, String version, int size) { // Later may be version as int and osetrit vstup
        this.name = name;
        this.version = version;
        try {
            if (size > 100000)
                throw new IllegalArgumentException("Size of the module " + name + " is too big. Limit is 100MB");
            this.size = size;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    public Module() {
        this.name = "Unknown";
        this.version = "0.0.0";
        this.size = 0;
    }

    @Override
    public String toString() {
        return name + " (" + version + ", " + size + " kB)";
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }
}
