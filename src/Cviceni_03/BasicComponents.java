package Cviceni_03;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Random;

public class BasicComponents extends Application {
    private FlowPane root;
    private final String[] SHORT_TEXTS = {
            "Holka modrooká, nesedávej u potoka, \n" +
                    "holka modrooká, nesedávej tam: \n" +
                    "v potoce je velká voda, vezme-li tě, bude škoda; \n" +
                    "holka modrooká, nesedávej tam!",

            "Holka modrooká, nesedávej u potoka, \n" +
                    "holka modrooká, nesedávej tam: \n" +
                    "v potoce se voda točí, podemele tvoje oči; \n" +
                    "holka modrooká, nesedávej tam!",

            "Holka modrooká, nesedávej u potoka, \n" +
                    "holka modrooká, nesedávej tam: \n" +
                    "přijde na tě mysliveček, připraví tě o věneček; \n" +
                    "holka modrooká, nesedávej tam!",

            "Bílým šátkem mává, \n" +
                    "kdo se loučí, \n" +
                    "každého dne se něco končí, \n" +
                    "něco překrásného se končí...",

            "Setři si slzy a usměj se \n" +
                    "uplakanýma očima, \n" +
                    "každého dne se něco počíná, \n" +
                    "něco překrásného se počíná.\n"};

    private final String[] WINDOW_TITLES = {"Basic title", "Another title", "Unknown title",
            "Super title", "Main title"};

    private final String[] SCENE_LABLES = {
            "Basic label",
            "Short label",
            "Another label",
            "Additional label",
            "Great label"
    };

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new FlowPane();
        Button[] buttons = new Button[]{
                new Button("Initial button"),
                new Button("Print text"),
                new Button("Change title"),
                new Button("Add text"),
                new Button("Hide buttons")
        };
        Random rnd = new Random();

        buttons[0].setOnAction(event -> Arrays.stream(buttons)
                .forEach(button -> button.setVisible(true)));

        buttons[1].setOnAction(event ->
                System.out.println(SHORT_TEXTS[rnd.nextInt(SHORT_TEXTS.length)]));

        buttons[2].setOnAction(event -> primaryStage
                .setTitle(WINDOW_TITLES[rnd.nextInt(WINDOW_TITLES.length)]));

        buttons[3].setOnAction(event -> root.getChildren()
                .add(new Label(SCENE_LABLES[rnd.nextInt(SCENE_LABLES.length)])));

        buttons[4].setOnAction(event -> Arrays.stream(buttons)
                .filter(button -> !button.equals(buttons[0]))
                .forEach(button -> button.setVisible(false)));
        buttons[4].fire(); // Abych si nepsal zbytecne kod skryvajici ostatni tlacitka pri startu.

        root.getChildren().addAll(buttons);

        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.setTitle("Cviceni_03");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
