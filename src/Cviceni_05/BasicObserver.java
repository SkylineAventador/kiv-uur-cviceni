package Cviceni_05;

import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class BasicObserver extends Application {
    Servant servant;

    @Override
    public void start(Stage primaryStage) throws Exception {
        servant = new Servant(new BorderPane(), primaryStage, 800, 600,
                0, 255, 0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
