package Cviceni_05;

import javafx.scene.control.TextField;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ObservingTextField extends TextField implements PropertyChangeListener {

    public ObservingTextField(String text) {
        super(text);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        setText(evt.getNewValue().toString());
    }
}
