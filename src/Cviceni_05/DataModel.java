package Cviceni_05;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class DataModel {
    private IntegerProperty colorValue_R;
    private IntegerProperty colorValue_G;
    private IntegerProperty colorValue_B;

    private StringProperty R_StringValue;
    private StringProperty G_StringValue;
    private StringProperty B_StringValue;

    private int min, max;

    private final PropertyChangeSupport listenerManager;

    public IntegerProperty getColorValue(char color) {
        switch (color) {
            case 'R':{
                return colorValue_R;
            }
            case 'G':{
                return colorValue_G;
            }
            case 'B':{
                return colorValue_B;
            }
            default:{
                return colorValue_R;
            }
        }
    }

    public DataModel(IntegerProperty colorValue_R, IntegerProperty colorValue_G, IntegerProperty colorValue_B, int min, int max) {
        this.colorValue_R = colorValue_R;
        this.colorValue_G = colorValue_G;
        this.colorValue_B = colorValue_B;
        this.min = min;
        this.max = max;
        this.R_StringValue = new SimpleStringProperty("Red value is: " + colorValue_R.get());
        this.G_StringValue = new SimpleStringProperty("Green value is: " + colorValue_G.get());
        this.B_StringValue = new SimpleStringProperty("Blue value is: " + colorValue_B.get());

        listenerManager = new PropertyChangeSupport(this);
    }

    public int getColorValue_R() {
        return colorValue_R.get();
    }

    public IntegerProperty colorValue_RProperty() {
        return colorValue_R;
    }

    public int getColorValue_G() {
        return colorValue_G.get();
    }

    public IntegerProperty colorValue_GProperty() {
        return colorValue_G;
    }

    public int getColorValue_B() {
        return colorValue_B.get();
    }

    public IntegerProperty colorValue_BProperty() {
        return colorValue_B;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listenerManager.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listenerManager.removePropertyChangeListener(listener);
    }

    public String getR_StringValue() {
        return R_StringValue.get();
    }

    public StringProperty r_StringValueProperty() {
        return R_StringValue;
    }

    public String getG_StringValue() {
        return G_StringValue.get();
    }

    public StringProperty g_StringValueProperty() {
        return G_StringValue;
    }

    public String getB_StringValue() {
        return B_StringValue.get();
    }

    public StringProperty b_StringValueProperty() {
        return B_StringValue;
    }
}
