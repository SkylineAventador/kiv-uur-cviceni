package Cviceni_05;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

public class Servant {
    private final int[] DIMENSION;
    private final int INIT,MAX, MIN;
    private Rectangle rectangle = new Rectangle();

    DataModel dataModel;
    BorderPane pane;


    Servant(Pane mainPane, Stage primaryStage, int windowWidth, int widnowHeight,
            int INIT, int MAX, int MIN) {
        DIMENSION = new int[]{windowWidth, widnowHeight};
        this.INIT = INIT;
        this.MAX = MAX;
        this.MIN = MIN;
        this.pane = (BorderPane) mainPane;
        configStage(primaryStage, this.pane);
        configPane(this.pane);
        primaryStage.show();
    }

    private VBox getCenterContent() {

        Slider[] sliders = new Slider[]{new Slider(MIN, MAX, INIT),
                new Slider(MIN, MAX, INIT),
                new Slider(MIN, MAX, INIT)};

        Label[] labels = new Label[]{
                new Label("Red value is: "),
                new Label("Green value is: "),
                new Label("Blue value is: ")
        };

        dataModel = new DataModel(
                new SimpleIntegerProperty(INIT),
                new SimpleIntegerProperty(INIT),
                new SimpleIntegerProperty(INIT), MIN, MAX);

        ObservingTextField TF_Red = new ObservingTextField(dataModel.colorValue_RProperty().toString());
        ObservingTextField TF_Green = new ObservingTextField(dataModel.colorValue_GProperty().toString());
        ObservingTextField TF_Blue = new ObservingTextField(dataModel.colorValue_BProperty().toString());


        for (Slider slider :
                sliders) {
            slider.setShowTickMarks(true);
            slider.setShowTickLabels(true);
            slider.setMinorTickCount(4);
        }


        TF_Red.textProperty().bindBidirectional(dataModel.colorValue_RProperty(), new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string.matches("\\d+")) {
                    return Integer.parseInt(string);
                } else {
                    return 0;
                }
            }
        });

        TF_Green.textProperty().bindBidirectional(dataModel.colorValue_GProperty(), new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string.matches("\\d+")) {
                    return Integer.parseInt(string);
                } else {
                    return 0;
                }
            }
        });

        TF_Blue.textProperty().bindBidirectional(dataModel.colorValue_BProperty(), new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string.matches("\\d+")) {
                    return Integer.parseInt(string);
                } else {
                    return 0;
                }
            }
        });



        sliders[0].valueProperty().bindBidirectional(dataModel.colorValue_RProperty());
        sliders[1].valueProperty().bindBidirectional(dataModel.colorValue_GProperty());
        sliders[2].valueProperty().bindBidirectional(dataModel.colorValue_BProperty());

        HBox[] hBoxes = new HBox[]{
                new HBox(labels[0], TF_Red),
                new HBox(labels[1], TF_Green),
                new HBox(labels[2], TF_Blue)
        };

        rectangle.setFill(Color.BLACK);
        rectangle.setWidth(150);
        rectangle.setHeight(150);
        rectangle.fillProperty().bind(Bindings.createObjectBinding(
                () -> Color.rgb(dataModel.getColorValue_R(), dataModel.getColorValue_G(), dataModel.getColorValue_B()),
                dataModel.colorValue_RProperty(), dataModel.colorValue_GProperty(), dataModel.colorValue_BProperty()));

        Group rect = new Group(rectangle);
        VBox rectVBox = new VBox(rect);
        rectVBox.setAlignment(Pos.CENTER);

        VBox content_Box = new VBox(hBoxes[0], sliders[0], hBoxes[1], sliders[1], hBoxes[2], sliders[2], rectVBox);
        content_Box.setSpacing(10);
        return content_Box;
    }

    private HBox getBottomContent() {
        HBox result = new HBox();
        Button exitButton = new Button("Exit");
        exitButton.setOnAction(event -> Platform.exit());
        result.getChildren().add(exitButton);
        result.setAlignment(Pos.CENTER);
        result.setPadding(new Insets(10, 0, 10, 0));
        return result;
    }

    private void configBorderPane(BorderPane pane) {
        pane.setBottom(getBottomContent());
        pane.setCenter(getCenterContent());
    }

    private void configPane(Pane pane) {
        if (pane instanceof BorderPane) {
            configBorderPane((BorderPane) pane);
        } else {
            System.err.println("Error. BorderPane type is supported only yet. Please change it at the main method.");
            Platform.exit();
        }
    }

    private void configStage(Stage stage, Pane pane) {
        stage.setScene(new Scene(pane, DIMENSION[0], DIMENSION[1]));
        stage.setMinHeight(500);
        stage.setMinWidth(320);
        stage.setTitle("Cviceni_05 - Observable/Observer");
    }


}
