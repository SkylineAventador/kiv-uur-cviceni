package Cviceni_08;

public class Product {
    private String productName;
    private ProductType productType;

    public Product(String productName, ProductType productType) {
        this.productName = productName;
        this.productType = productType;
    }

    public Product(String productName) {
        this.productName = productName;
        this.productType = ProductType.CATEGORY;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductType() {
        return productType.toString();
    }

    public ProductType getEnumProductType() {
        return productType;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}