package Cviceni_08;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.Optional;

public class BasicTree extends Application {
    public static final String DEF_LANGUAGE = "cs";
    public static final String DEF_COUNTRY = "CZ";

    private Stage primaryStage;
    private TreeView<Product> product_TV;
    private TextArea details_TA;
    private TextField name_TF;
    private ChoiceBox<ProductType> type_CB;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));

        this.primaryStage.setTitle("Tree example");
        this.primaryStage.setScene(createScene());
        this.primaryStage.setMinWidth(590);
        this.primaryStage.setMinHeight(200);
        this.primaryStage.show();
    }

    private Scene createScene() {
        Scene scene = new Scene(getRoot(), 800, 600);
        return scene;
    }

    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();

        rootPane.setCenter(getTree());
        rootPane.setBottom(getControls());

        rootPane.setPadding(new Insets(0, 0, 5, 0));
        return rootPane;
    }

    private Node getTree() {
        product_TV = new TreeView<Product>();

        product_TV.setCellFactory(treeView -> new ProductDisplayCell());

        product_TV.setEditable(true);

        product_TV.getSelectionModel().selectedItemProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {

            TreeItem<Product> selectedItem = (TreeItem<Product>) newValue;
                details_TA.setText("Node path: " + buildNodePath());
        });

        BorderPane.setMargin(product_TV, new Insets(5));
        product_TV.setRoot(new TreeItem<>(new Product("Product")));

//        Generates default children for this root node.
        createDefaultChildren(product_TV.getRoot());

        return product_TV;
    }

    private String buildNodePath() {
        StringBuilder pathBuilder = new StringBuilder();
        for (TreeItem<Product> item = product_TV.getSelectionModel().getSelectedItem();
             item != null ; item = item.getParent()) {

            pathBuilder.insert(0, item.getValue().getProductName());
            if (item != product_TV.getRoot())pathBuilder.insert(0, " -> ");
        }
        return pathBuilder.toString();
    }

    private void createDefaultChildren(TreeItem<Product> parent) {
        TreeItem<Product> electroBranch = new TreeItem<>(new Product("Electrical"));
        TreeItem<Product> nonElectroBranch = new TreeItem<>(new Product("Non_Electrical"));

        TreeItem<Product> computer = new TreeItem<>(new Product("Computer", ProductType.ELECTRICAL));
        TreeItem<Product> smartphone = new TreeItem<>(new Product("Smartphone", ProductType.ELECTRICAL));

        TreeItem<Product> table = new TreeItem<>(new Product("Table", ProductType.NON_ELECTRICAL));
        TreeItem<Product> chair = new TreeItem<>(new Product("Chair", ProductType.NON_ELECTRICAL));

        electroBranch.getChildren().addAll(computer, smartphone);
        nonElectroBranch.getChildren().addAll(table, chair);

        parent.getChildren().addAll(electroBranch, nonElectroBranch);
    }

    private Node getControls() {
        HBox result = new HBox(5);

        Label name_L = new Label("Name:");
        Label type_L = new Label("Type:");

        name_TF = new TextField();
        name_TF.setPromptText("Enter name of a product.");

        type_CB = new ChoiceBox<ProductType>(FXCollections.observableArrayList(ProductType.values()));
        type_CB.getSelectionModel().selectFirst();

        Button add_B = new Button("Add");
        add_B.setOnAction(event -> addProduct());

        Button remove_B = new Button("Remove");
        remove_B.setOnAction(event -> removeProduct());

        details_TA = new TextArea("Node path:");
        details_TA.setMaxWidth(300);
        details_TA.setMaxHeight(100);
        details_TA.setWrapText(true);
        details_TA.setEditable(false);

        VBox name_Vbox = new VBox(name_L, name_TF);
        VBox category_Vbox = new VBox(type_L, type_CB);
        VBox buttons_Vbox = new VBox(add_B, remove_B);
        VBox details_Vbox = new VBox(details_TA);
        buttons_Vbox.setSpacing(5);

        result.getChildren().addAll(name_Vbox, category_Vbox, buttons_Vbox, details_Vbox);
        result.setAlignment(Pos.CENTER);
        result.setSpacing(10);

        return result;
    }

    private void addProduct() {
        if (!name_TF.getText().isEmpty() && !checkNameDuplicates()) {
            insertItem(product_TV.getSelectionModel().getSelectedItem());
        } else {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Error");
            error.setHeaderText("Error has been occured!");
            error.setContentText("Name of a product is empty or product with entered name already exists.");
            error.showAndWait();
        }
    }

    private boolean checkNameDuplicates() {
        for (TreeItem<Product> item = product_TV.getSelectionModel().getSelectedItem();
             item != null ; item = item.getParent()) {
            if (name_TF.getText().equals(item.getValue().getProductName()))
                return true;
        }
        return false;
    }

    private void removeProduct() {
        if (!product_TV.getSelectionModel().isEmpty()) {
            TreeItem<Product> selected = product_TV.getSelectionModel().getSelectedItem();
            if (selected.getParent() == null) {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setTitle("Error");
                error.setHeaderText("Error has been occured!");
                error.setContentText("Root of the tree can not be deleted. Please select another one item.");
                error.showAndWait();
            } else if (selected.getValue().getProductType().equals(ProductType.CATEGORY.toString())) {
                Alert error = new Alert(Alert.AlertType.CONFIRMATION);
                error.setTitle("Warning");
                error.setHeaderText("Remove confirmation!");
                error.setContentText("Are you sure want to delete the whole category?\n" +
                        "All items under this node will be deleted.\nThis action can not be undone.");
                Optional<ButtonType> result = error.showAndWait();
                if(!result.isPresent())
                // alert is exited, no button has been pressed.
                    System.out.println("No button has been pressed. Action has been canceled.");
                else if(result.get() == ButtonType.OK)
                //oke button is pressed
                    removeItem(product_TV.getSelectionModel().getSelectedItem());
                else if(result.get() == ButtonType.CANCEL)
                    // cancel button is pressed
                    System.out.println("Action has been canceled.");

            } else {
                Alert error = new Alert(Alert.AlertType.CONFIRMATION);
                error.setTitle("Warning");
                error.setHeaderText("Remove confirmation!");
                error.setContentText("Selected node and all items under it will be deleted." +
                        "\nThis action can not be undone.");
                // Event listener here -> confirm removing
                Optional<ButtonType> result = error.showAndWait();
                if(!result.isPresent())
                    // alert is exited, no button has been pressed.
                    System.out.println("No button has been pressed. Action has been canceled.");
                else if(result.get() == ButtonType.OK)
                    //oke button is pressed
                    removeItem(product_TV.getSelectionModel().getSelectedItem());
                else if(result.get() == ButtonType.CANCEL)
                    // cancel button is pressed
                    System.out.println("Action has been canceled.");
            }
        } else {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Error");
            error.setHeaderText("Error has been occured!");
            error.setContentText("At least one item must be selected to remove it from the tree.");
            error.showAndWait();
        }
    }

    private void removeItem(TreeItem<Product> item) {
        item.getParent().getChildren().remove(item);
    }

    private void insertItem(TreeItem<Product> destination) {
        String formatName = name_TF.getText().trim();
        formatName = formatName.replaceFirst(formatName.substring(0, 1),
                formatName.substring(0, 1).toUpperCase());
        if (destination.getValue().getEnumProductType() != ProductType.CATEGORY) {
            destination.getChildren().add(
                    new TreeItem<>(new Product(formatName,
                            destination.getValue().getEnumProductType()))
            );
        } else if (destination.equals(product_TV.getRoot())) {
            destination.getChildren().add(
                    new TreeItem<>(new Product(formatName,
                            ProductType.CATEGORY))
            );
        } else {
            destination.getChildren().add(
                    new TreeItem<>(new Product(formatName,
                            ProductType.valueOf(destination.getValue().getProductName().toUpperCase())))
            );
        }
    }
}
