package Cviceni_08;

import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * Jelikoz je to ciste technicka trida, zkopiroval jsem veskery content ze vzorove vase tridy.
 * @author Richard Lipka
 */
public class ProductDisplayCell extends TreeCell<Product> {
    // TextField that serves as an editor
    private TextField textTF;

    // method switching cell into the editation state
    public void startEdit() {
        super.startEdit();

        // creates editor if it is not already available
        if (textTF == null) {
            createEditor();
        }

        // disabling renderer
        setText(null);
        // setting up editor - creatign content
        textTF.setText(createEditorContent());
        // displaying editor in the cell
        setGraphic(textTF);
    }

    // method switching cell into displaying state
    public void cancelEdit() {
        super.cancelEdit();

        // setting content of the renderer (label)
        setText(createContent());
        // removing editor from the cell
        setGraphic(null);
    }

    // method setting updated value to the cell
    public void updateItem(Product item, boolean empty) {
        super.updateItem(item, empty);

        // nothing is displayed for empty cell
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            // in editation state
            if (isEditing()) {
                if (textTF != null) {
                    // setting content of the editor
//                    textTF.setText(createEditorContent());
                    textTF.setText(createEditorContent());
                    // disabling renderer
                    setText(null);
                    // adding editor to the scene graph
                    setGraphic(textTF);
                }
                // in displaying state
            } else {
                // creating content of the renderer
                setText(createContent());
                // disabling editor
                setGraphic(null);
            }
        }
    }
    // creates content for the renderer
    private String createContent() {
        return getPrefixSymbol(getItem().getEnumProductType()) + " " + getItem().getProductType() + ": " + getItem().getProductName();
    }

    private char getPrefixSymbol(ProductType productType) {
        if (productType == ProductType.CATEGORY)
            return '\u058D';
        else if (productType == ProductType.ELECTRICAL)
            return '\u2607';
        else return '\u274C';
    }

    // creates content for the editor
    private String createEditorContent() {
        // editor displays only name - gender symbol is not part of the name that
        // is changed in editor
        return getItem().getProductName();
    }

    // creates editor itself
    private void createEditor() {
        // editor is based on the text field
        textTF = new TextField();
        // adding reaction to the pressed key, in order to determine if the
        // new value was commited or if the editation was canceled
        textTF.setOnKeyReleased(event -> {
            // when editation is confirmed
            if (event.getCode() == KeyCode.ENTER) {
                // veryfying if new value was really provided
                if (textTF.getText().length() == 0) {
                    // when no value was provided, editation is canceled
                    cancelEdit();
                    // when new value was provided and confirmed
                } else {
                    // getting acces to the element from the model, representing
                    // edited value
                    Product product = getItem();
                    // new name is set to the model
                    product.setProductName(textTF.getText());
                    // commit event is fired
                    commitEdit(product);
                }
                // when editation is canceled
            } else if (event.getCode() == KeyCode.ESCAPE) {
                // cancel event is fired
                cancelEdit();
            }
        });
    }
}
