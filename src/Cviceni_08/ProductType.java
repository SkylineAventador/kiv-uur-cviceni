package Cviceni_08;

public enum ProductType {
    ELECTRICAL, NON_ELECTRICAL, CATEGORY;

    public String toString(){
        if (this == ELECTRICAL) {
            return "Electrical";
        } else if (this == NON_ELECTRICAL) {
            return "Non_Electrical";
        }
        return "Category";
    }

}
