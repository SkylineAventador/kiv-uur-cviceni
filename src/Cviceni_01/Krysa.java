package Cviceni_01;

public class Krysa extends Savec{
    public Krysa(String birthName) {
        super(birthName);
        this.FOOD_NAME = "Odpadky";
        this.FOOD_ENERGY_POINTS = 2;
    }

    @Override
    public boolean cry() {
        if (super.cry()){
            System.out.println("Krysa " + getName() + " pisti.");
        }
        return false;
    }
}
