package Cviceni_01;

/**
 * Rozhrani pro spolecne vlastnosti vsech tvoru.
 */
public interface ICreature {
    /**
     * Pohyb tvoru do zadanych souradnic x a y.
     * @param x souradnice X.
     * @param y souradnice Y.
     */
    boolean moveTo(int x, int y);

    /**
     * Nakrmi tvor a prida energii.
     */
    void eat();

    /**
     * Metoda vypisuje podrobne informace o stavu zvirete.
     */
    void getInfo();

    /**
     * Jmeno zvirete ktere mu bude prirazeno pri jehoz vytvoreni.
     */
    void setInitName(String name);
}
