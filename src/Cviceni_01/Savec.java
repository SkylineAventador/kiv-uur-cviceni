package Cviceni_01;

public abstract class Savec extends Creature implements IHlucna {
    public Savec(String birthName) {
        super(birthName);
    }

    @Override
    public boolean moveTo(int x, int y) {
        if(super.moveTo(x, y)) {

            if (getClass().getSimpleName().equals("Krysa")) {
                System.out.println(getName() + " bezi do pozice x = " + x + ", y = " + y + ".");
            } else {
                System.out.println(getName() + " se plouzi do pozice x = " + x + ", y = " + y + ".");
            }
        }
        return false;
    }

    public boolean cry(){
        if (getEnergyPoints() >= 1) {
            setEnergyPoints(getEnergyPoints() - 1);
            return true;
        } else {
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " je unaven(a). Nemuze kricet. Nakrmte ho/ji.");
            return false;
        }
    }

    @Override
    public void eat() {
        setEnergyPoints(getEnergyPoints() + this.FOOD_ENERGY_POINTS);
        System.out.println(getName() + " se najedl(a) " + this.FOOD_NAME +
                "(+" + this.FOOD_ENERGY_POINTS + ") a ted ma "
                + getEnergyPoints() + " bodu energie.");
    }
}
