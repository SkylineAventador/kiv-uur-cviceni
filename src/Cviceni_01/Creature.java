package Cviceni_01;

public abstract class Creature implements ICreature {
    private int x = 0, y = 0;

    /**
     * Kapacita energie zvirete. Defaultne nastaveno na 2, aby melo zvirete
     * energii alespon na jeden posun.
     */
    private int energyPoints = 5;

    private String name = "Nezname zviratko";
    public String FOOD_NAME = "Unknown";
    public int FOOD_ENERGY_POINTS = 0;

    public Creature(String birthName) {
        setInitName(birthName);
        System.out.println(this.getClass().getSimpleName() + " " + getName() + " byl(a) vytvoren(a).");
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setEnergyPoints(int energyPoints) {
        this.energyPoints = energyPoints;
    }

    public String getName() {
        return name;
    }

    public int getEnergyPoints() {
        return energyPoints;
    }

    @Override
    public boolean moveTo(int x, int y) {
        if (x < 0 || y < 0) {
            System.out.println("Souradnice posunu pro "+ getName() +" nemohou " +
                    "mit zapornou hodnotu!");
            return false;
        } else {
            if (this.energyPoints - 1 < 0) {
                System.out.println(getName() + " je unaven(a). Nema energii a se nemuze pohybovat. Nakrmte ho.");
                return false;
            } else {

                setX(x);
                setY(y);

                setEnergyPoints(getEnergyPoints() - 1);

                return true;
            }
        }
    }

    @Override
    public abstract void eat();

    @Override
    public void getInfo() {
        System.out.println("=== Info o zvireti ===");
        System.out.println("Jmeno zvirete: " + getName());
        System.out.println("Pozice zvirete: x = " + getX() + ", y = " + getY());
        System.out.println("Zbyvajici energie: " + getEnergyPoints());
        System.out.println("======================");
    }

    @Override
    public void setInitName(String name) {
        this.name = name;
    }
}
