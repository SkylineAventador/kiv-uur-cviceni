package Cviceni_01;

public class Lenochod extends Savec {
    public Lenochod(String birthName) {
        super(birthName);
        this.FOOD_NAME = "Listi";
        this.FOOD_ENERGY_POINTS = 3;
    }


    @Override
    public boolean cry() {
        if (super.cry()){
            System.out.println("Lenochod " + getName() + " bruci.");
        }
        return false;
    }
}
