package Cviceni_01;

public abstract class Ptak extends Creature implements IHlucna, IVejcoroda {
    public Ptak(String creatureName) {
        super(creatureName);
    }

    @Override
    public boolean moveTo(int x, int y) {
        if (super.moveTo(x, y)) {

            if (getClass().getSimpleName().equals("Orel")) {
                System.out.println(getName() + " vysoko leti do pozice x = " + x + ", y = " + y + ".");
            } else {
                System.out.println(getName() + " nizko leti do pozice x = " + x + ", y = " + y + ".");
            }
        }
        return false;
    }

    @Override
    public boolean cry() {
        if (getEnergyPoints() >= 1) {
            setEnergyPoints(getEnergyPoints() - 1);
            return true;
        } else {
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " je unaven(a). Nemuze kricet. Nakrmte ho/ji.");
            return false;
        }
    }

    @Override
    public void eat() {
        setEnergyPoints(getEnergyPoints() + this.FOOD_ENERGY_POINTS);
        System.out.println(getName() + " se najedl(a) " + this.FOOD_NAME +
                "(+" + this.FOOD_ENERGY_POINTS + ") a ted ma "
                + getEnergyPoints() + " bodu energie.");
    }

    @Override
    public boolean layEggs() {
        if (getEnergyPoints() >= 5) {
            setEnergyPoints(getEnergyPoints() - 5);
            return true;
        } else {
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " je unaven(a). Nemuze snaset vejce. Nakrmte ho/ji");
            return false;
        }
    }
}
