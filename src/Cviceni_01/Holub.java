package Cviceni_01;

public class Holub extends Ptak{

    public Holub(String creatureName) {
        super(creatureName);
        this.FOOD_NAME = "Zrno";
        this.FOOD_ENERGY_POINTS = 3;
    }

    @Override
    public boolean cry() {
        if (super.cry()){
            System.out.println("Holub " + getName() + " pipa.");
        }
        return false;
    }

    @Override
    public boolean layEggs() {
        if(super.layEggs()) {
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " snesl(a) 2 vejce.");
        }
        return true;
    }
}
