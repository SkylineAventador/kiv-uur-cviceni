package Cviceni_01;

public class Barakuda extends Ryba{

    public Barakuda(String creatureName) {
        super(creatureName);
        this.FOOD_NAME = "Maso";
        this.FOOD_ENERGY_POINTS = 5;
    }

    @Override
    public boolean layEggs() {
        if(super.layEggs()){
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " snesl(a) 1000 vajec.");
        }
        return false;
    }
}
