package Cviceni_01;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Kravtsov Dmytro A18B0245P
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("=================Vytvareni======================");
        Lenochod lenochod_1 = new Lenochod("Lenochod_1");
        Lenochod lenochod_2 = new Lenochod("Lenochod_2");

        Krysa krysa_1 = new Krysa("Krysa_1");
        Krysa krysa_2 = new Krysa("Krysa_2");

        Orel orel_1 = new Orel("Orel_1");
        Orel orel_2 = new Orel("Orel_2");

        Holub holub_1 = new Holub("Holub_1");
        Holub holub_2 = new Holub("Holub_2");

        Barakuda barakuda_1 = new Barakuda("Barakuda_1");
        Barakuda barakuda_2 = new Barakuda("Barakuda_2");

        Kapr kapr_1 = new Kapr("Kapr_1");
        Kapr kapr_2 = new Kapr("Kapr_2");

        System.out.println("==========================================\n");

        ArrayList<Creature> allAnimals = new ArrayList<Creature>(12);
        allAnimals.add(lenochod_1);
        allAnimals.add(lenochod_2);
        allAnimals.add(krysa_1);
        allAnimals.add(krysa_2);
        allAnimals.add(orel_1);
        allAnimals.add(orel_2);
        allAnimals.add(holub_1);
        allAnimals.add(holub_2);
        allAnimals.add(barakuda_1);
        allAnimals.add(barakuda_2);
        allAnimals.add(kapr_1);
        allAnimals.add(kapr_2);

        ArrayList<IVejcoroda> eggsOnly = new ArrayList<>(12);
        for (Creature creature: allAnimals
             ) {
            if (creature.getClass().getSuperclass().getSimpleName().equals("Ryba") ||
                    creature.getClass().getSuperclass().getSimpleName().equals("Ptak")) {
                eggsOnly.add((IVejcoroda)creature);
            }
        }
        eggsOnly.trimToSize();

        ArrayList<IHlucna> loudOnly = new ArrayList<>(12);
        for (Creature creature: allAnimals
        ) {
            if (creature.getClass().getSuperclass().getSimpleName().equals("Ptak") ||
                    creature.getClass().getSuperclass().getSimpleName().equals("Savec")) {
                loudOnly.add((IHlucna)creature);
            }
        }
        loudOnly.trimToSize();

        ArrayList<Creature> birdsOnly = new ArrayList<Creature>(12);
        for (Creature creature: allAnimals
        ) {
            if (creature.getClass().getSuperclass().getSimpleName().equals("Ptak")) {
                birdsOnly.add(creature);
            }
        }
        birdsOnly.trimToSize();


        doActions(allAnimals,1);
        doActions(loudOnly,2);
        doActions(eggsOnly,3);
    }

    private static void doActions(ArrayList pole, int index) {
        switch (index) {
            case 1: {
                System.out.println("=================Krmeni======================");
                giveFood(pole);
                System.out.println("============================================\n");
                System.out.println("=================Pohyb======================");
                moveAnimal(pole);
                System.out.println("============================================\n");
                break;
            }
            case 2:{
                System.out.println("=====================Krik====================");
                loudAnimalsCry(pole);
                System.out.println("============================================\n");
                break;
            }
            case 3:{
                System.out.println("=====================Snaseni vajec==================");
                layEggs(pole);
                System.out.println("============================================\n");
                break;
            }
        }
    }

    private static void giveFood(ArrayList<Creature> pole) {
        for (Creature creature : pole) {
            if (pole.indexOf(creature) %3 != 0) {
                creature.eat();
            } else {
                creature.setEnergyPoints(0);
            }
        }
    }

    private static void moveAnimal(ArrayList<Creature> pole) {
        Random random = new Random();
        for (Creature creature : pole) {
            creature.moveTo(random.nextInt(100), random.nextInt(100));
        }
    }

    private static void loudAnimalsCry(ArrayList<IHlucna> pole) {
        for (IHlucna creature : pole) {
            creature.cry();
        }
    }

    private static void layEggs(ArrayList<IVejcoroda> pole) {
        for (IVejcoroda creature : pole) {
            creature.layEggs();
        }
    }
    //Komentar
}
