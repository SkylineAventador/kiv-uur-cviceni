package Cviceni_01;

public class Orel extends Ptak {
    public Orel(String birthName) {
        super(birthName);
        this.FOOD_NAME = "Maso";
        this.FOOD_ENERGY_POINTS = 5;
    }


    @Override
    public boolean cry() {
        if (super.cry()) {
            System.out.println("Orel " + getName() + " hlucne krici.");
        }
        return false;
    }

    @Override
    public boolean layEggs() {
        if (super.layEggs()) {
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " snesl(a) 3 vejce.");
        }
        return true;
    }
}
