package Cviceni_01;

public class Kapr extends Ryba {

    public Kapr(String birthName) {
        super(birthName);
        this.FOOD_NAME = "Rasy";
        this.FOOD_ENERGY_POINTS = 3;
    }


    @Override
    public boolean layEggs() {
        if(super.layEggs()){
            System.out.println(getClass().getSimpleName() + " " + getName() +
                    " snesl(a) 500 vajec.");
        }
        return false;
    }
}
