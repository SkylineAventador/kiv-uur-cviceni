package Cviceni_02;

/**
 * @author Kravtsov Dmytro A18B0245P
 * Hlavni trida programu 2. cviceni KIV/UUR
 */
public class Main {
    public static void main(String[] args) {
        ServiceBot serviceBot = new ServiceBot();

        //=========Blok_1=================
        serviceBot.createBirds(6);
        serviceBot.moveBirds();
        //================================

        //=========Blok_2=================
        serviceBot.exec_blok_2();
        //================================

        //=========Blok_3=================
        serviceBot.exec_blok_3();
        //================================

        //=========Blok_4=================
        serviceBot.exec_blok_4();
        //================================

        //=========Blok_5=================
        serviceBot.exec_blok_5();
        //================================

        //=========Blok_6=================
        serviceBot.exec_blok_6();
        //================================
    }
}
