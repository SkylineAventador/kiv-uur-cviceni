package Cviceni_02;

import Cviceni_01.*;

import java.util.*;

/**
 * Obsluzna trida aplikace. Pouziva se pro volani odpovidajicich metod,
 * ktere provedou akce podle zadani a vypisou vysledek na obrazovku.
 * @author Kravtsov Dmytro A18B0245P
 */
public class ServiceBot {
    private ArrayList<Creature> creatures = new ArrayList<>(6);
    private ArrayList<String> names;

    private void printSeparatorHeader(int taskNumber, int blockNumber) {
        System.out.println("\n==========Ukol_" + taskNumber + "_Blok_" + blockNumber + "==========");
    }

    private void printSeparatorFooter() {
        System.out.println("==============================\n");
    }

    /**
     * Vytvoreni kolekci ptaku v urcitem poctu.
     * @param capacity pocet ptaku ve vysledne kolekci.
     */
    public void createBirds(int capacity) {
        printSeparatorHeader(1, 1);
        for (int i = 0; i < capacity; i++) {
            if (i % 2 == 0) {
                creatures.add(new Orel("Orel_" + (i + 1)));
            } else {
                creatures.add(new Holub("Holub_" + (i + 1)));
            }
        }
        System.out.println("Birds collection has been created. Capacity of birds inside: " + creatures.size());
        printSeparatorFooter();
    }

    public void moveBirds() {
        printSeparatorHeader(2, 1);
        Random rnd = new Random();
        creatures.forEach(creature -> creature.moveTo(rnd.nextInt(100), rnd.nextInt(100)));
        printSeparatorFooter();
    }

    public void exec_blok_2() {
        creatures.clear();

        printSeparatorHeader(1,2);
        for (int i = 0; i < 6; i++) {
            if (i < 2) {
                creatures.add(new Orel("Orel_" + (i + 1) + "_Blok2"));
            } else if (i < 4) {
                creatures.add(new Barakuda("Barakuda_" + (i + 1) + "_Blok2"));
            } else {
                creatures.add(new Lenochod("Lenochod_" + (i + 1) + "_Blok2"));
            }
        }
        printSeparatorFooter();

        printSeparatorHeader(2,2);
        System.out.println("Nazev tridy prvniho prvku z kolekce: "
                + creatures.get(0).getClass().getSimpleName());
        printSeparatorFooter();

        printSeparatorHeader(3,2);
        System.out.println("Energie prvni barakudy: " + creatures.get(2).getEnergyPoints() + " bodu.");
        printSeparatorFooter();

        printSeparatorHeader(4,2);
        System.out.println("Je 1. a 2. prvek kolekce typu Ptak:\n1. - "
                + (creatures.get(0) instanceof Ptak) + ".\n2. - " + (creatures.get(1) instanceof Ptak) + ".");
        printSeparatorFooter();
    }

    public void exec_blok_3() {
        creatures.clear();

        char[] chars = new char[]{'A', 'B', 'C'};
        Random rnd = new Random();
        printSeparatorHeader(1, 3);
        for (int i = 0; i < 6; i++) {
            if (i < 2) {
                creatures.add(new Krysa("Savec_Krysa_" + (i + 1)));
            } else if (i % 2 == 0) {
                creatures.add(new Lenochod("Savec_Lenochod_" +
                        chars[rnd.nextInt(chars.length)]+
                        chars[rnd.nextInt(chars.length)]+
                        chars[rnd.nextInt(chars.length)]+ "_" +(i+1)));
            } else {
                creatures.add(new Krysa("Savec_Krysa_" +
                        chars[rnd.nextInt(chars.length)]+
                        chars[rnd.nextInt(chars.length)]+
                        chars[rnd.nextInt(chars.length)]+ "_" +(i+1)));
            }
        }
        //Printing all mammals
        creatures.forEach(creature -> System.out.println(creature.getName()));
        printSeparatorFooter();

        //Printing ABC mammals only.
        printSeparatorHeader(2, 3);
        creatures.stream().filter(creature -> creature.getName().contains("A") ||
                creature.getName().contains("B") ||
                creature.getName().contains("C")).
                forEach(creature -> System.out.println(creature.getName()));
        printSeparatorFooter();

        //Printing ABC mammals with more than 3 points of energy only.
        printSeparatorHeader(3,3);
        creatures.stream().filter(creature -> creature.getName().contains("A") ||
                                              creature.getName().contains("B") ||
                                              creature.getName().contains("C")).
                           filter(creature -> creature.getEnergyPoints() > 3).
                           forEach(creature -> System.out.println(creature.getName() +
                                               " Energy: " + creature.getEnergyPoints()));
        printSeparatorFooter();
    }

    public void exec_blok_4() {
        creatures.clear();

        printSeparatorHeader(1,4);
        for (int i = 0; i < 6; i++) {
            if (i % 2 == 0) {
                creatures.add(new Orel("Orel_" + (i + 1)));
                creatures.get(i).eat();
                creatures.get(i).eat();
            } else {
                creatures.add(new Holub("Holub_" + (i + 1)));
            }
        }

        creatures.stream().filter(creature -> creature.getEnergyPoints() > 10)
                .forEach(creature -> System.out.println(creature.getName()
                        + " Energy: " + creature.getEnergyPoints()));
        printSeparatorFooter();

        //Energy of all the birds.
        printSeparatorHeader(2,4);
        double averageEnergy = creatures.stream()
                .mapToDouble(Creature::getEnergyPoints)
                .average().getAsDouble();
        System.out.println("Prumerna energie vsech ptaku je: " + averageEnergy);
        printSeparatorFooter();

        //Energy of eagles only.
        printSeparatorHeader(3,4);
        double avgEnergyEagles = creatures.stream()
                .filter(creature -> creature instanceof Orel)
                .mapToDouble(Creature::getEnergyPoints)
                .average().getAsDouble();
        System.out.println("Prumerna energie vsech orlu je: " + avgEnergyEagles);
        printSeparatorFooter();
    }

    public void exec_blok_5() {
        names = new ArrayList<String>(6);
        String[] namesPool = {"Antonin","Petr","Milan","Jindriska","Dmytro","Marketa"};
        names.addAll(Arrays.asList(namesPool));
        printSeparatorHeader(1, 5);
        System.out.println("All names:");
        names.forEach(System.out::println);
        printSeparatorFooter();

        printSeparatorHeader(2, 5);
        System.out.println("ABC sorted:");
        Collections.sort(names);
        names.forEach(System.out::println);
        printSeparatorFooter();

        printSeparatorHeader(3, 5);
        Collections.shuffle(names);
        System.out.println("Shuffled names:");
        names.forEach(System.out::println);
        printSeparatorFooter();

        printSeparatorHeader(4, 5);
        System.out.println("Ascending:");
        names.stream()
                .sorted((name1, name2) -> {
                    if (name1.length() == name2.length()) {
                        return 0;
                    }
                    if (name1.length() <= name2.length()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }).forEach(System.out::println);
        printSeparatorFooter();

        printSeparatorHeader(5, 5);
        System.out.println("Descending:");
        names.stream()
                .sorted((name1, name2) -> {
                            if (name1.length() == name2.length()) {
                                return 0;
                            }
                            if (name1.length() >= name2.length()) {
                                return -1;
                            } else {
                                return 1;
                            }
        }).forEach(System.out::println);
        printSeparatorFooter();
    }

    public void exec_blok_6() {
        creatures.clear();
        for (int i = 0; i < 6; i++) {
            if (i % 2 == 0) {
                creatures.add(new Barakuda("Barakuda_" + (i + 1)));
            } else {
                creatures.add(new Kapr("Kapr_" + (i + 1)));
            }
        }

        printSeparatorHeader(1,6);
        creatures.forEach(creature -> System.out.println(creature.getName()));
        printSeparatorFooter();

        printSeparatorHeader(2, 6);
        ArrayList<Creature> creaturesCopy = creatures;
        System.out.println("New reference copy has been created.");
        printSeparatorFooter();

        printSeparatorHeader(3, 6);
        creatures.get(0).setEnergyPoints(0);
        System.out.println("Is reference functional: " + (creaturesCopy.get(0).getEnergyPoints() == 0));
        printSeparatorFooter();

        printSeparatorHeader(4, 6);
        creatures.add(4, new Kapr("Additional_Kapr"));
        System.out.println("Additional Kapr was added at index 4.");
        printSeparatorFooter();

        printSeparatorHeader(5, 6);
        creatures.removeIf(creature -> creature instanceof Barakuda);
        System.out.println("Fishes array without Barracudas:");
        creatures.forEach(creature -> System.out.println(creature.getName()));
        printSeparatorFooter();
    }
}
