package Cviceni_04;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ServiceBot {

//    private Robot robot;

    public void configStage(Stage stage, Pane pane) {
        stage.setScene(new Scene(pane, 800, 600));
        stage.setMinHeight(450);
        stage.setMinWidth(800);
        stage.setTitle("Cviceni_04 - Robot");
    }

    public void configBorderPane(BorderPane pane) {
        pane.setTop(getTopContent());
        pane.setLeft(getLeftContent());
        pane.setBottom(getBottomContent());
        pane.setRight(getRightContent());
    }

    private VBox getTopContent() {
        VBox result = new VBox();

        MenuBar menuBar = new MenuBar();
        Menu file_M = new Menu("File");
        MenuItem file_item = new MenuItem("Exit");
        file_item.setOnAction(event -> Platform.exit());
        file_M.getItems().add(file_item);

        Menu controls_M = new Menu("Controls");
        MenuItem[] controls_items = new MenuItem[]{
                new MenuItem("Download data"),
                new MenuItem("Update software"),
                new MenuItem("Upload file")
        };
        controls_M.getItems().addAll(controls_items);
        menuBar.getMenus().addAll(file_M, controls_M);

        Button[] controlButtons = new Button[]{
                new Button("Left"),
                new Button("Right"),
                new Button("Top"),
                new Button("Bottom"),
        };

        HBox topHBox = new HBox();
        topHBox.setSpacing(10);
        topHBox.setAlignment(Pos.CENTER);
        topHBox.getChildren().addAll(controlButtons);


        result.getChildren().addAll(menuBar, topHBox);
        result.setSpacing(10);
        result.setPadding(new Insets(0, 0, 10, 0));
        return result;
    }

    private VBox getLeftContent() {
        VBox result = new VBox();

        try {
            TextArea robotInfo_TA = new TextArea("Status: STAND_BY");
            robotInfo_TA.setEditable(false);
            robotInfo_TA.setPrefSize(300, 200);
            result.getChildren().add(robotInfo_TA);
            //Tim zpusobem zajistim (snad?), ze robot byl urcite vytvoren a kterakoliv metoda
            //se muze spolehnout, ze obdrzi referenci na nej. Jinak program zahlasi chybu jeste pred
            //prvnim vykreslenim okna a zavre se.
        } catch (NullPointerException e) {
            System.out.println("Robot nebyl vytvoren. Nemuzu vytvorit prislusnou cast okna.");
            Platform.exit();
        }
        Label robotInfo_Label = new Label("Info o robotovi:");
        result.getChildren().add(0, robotInfo_Label);
        result.setPadding(new Insets(10, 10, 10, 10));
        return result;
    }

    private HBox getBottomContent() {
        HBox result = new HBox(10);
        HBox leftPanel = new HBox(5), rightPanel = new HBox(5);

        VBox[] vBoxes = new VBox[4];
        Label[] dataTitles = new Label[]{new Label("Battery %"), new Label("Distance covered (m)"),
                new Label("Uptime (sec)"), new Label("Is ON")};
        TextField[] dataTextFields = new TextField[]{new TextField("100"), new TextField("123"),
                new TextField("1600"), new TextField("True")};
        for (TextField dataTextField : dataTextFields
        ) {
            dataTextField.setEditable(false);
        }

        for (int i = 0; i < vBoxes.length; i++) {
            vBoxes[i] = new VBox(dataTitles[i], dataTextFields[i]);
        }
        leftPanel.getChildren().addAll(vBoxes);

        Button[] controlButtons = new Button[]{new Button("ON"), new Button("OFF"), new Button("RESTART")};
        rightPanel.getChildren().addAll(controlButtons);
        rightPanel.setAlignment(Pos.BOTTOM_CENTER);

        result.getChildren().addAll(leftPanel, rightPanel);
        result.setPadding(new Insets(10, 10, 10, 10));
        return result;
    }

    private VBox getRightContent() {
        String[] sensorsNames = new String[]{"Acoustic", "Optical", "Temperature", "Pressure", "Radar"};
        String[] energySourcesNames = new String[]{"Solar", "Battery", "RTG"};
        CheckBox[] sensors = new CheckBox[sensorsNames.length];
        RadioButton[] sources = new RadioButton[energySourcesNames.length];
        ToggleGroup RB_Group = new ToggleGroup();
        Label title = new Label("Robot sensors:");
        Label sourcesTitle = new Label("Energy source:");

        for (int i = 0; i < sensors.length; i++) {
            sensors[i] = new CheckBox(sensorsNames[i]);
        }
        for (int i = 0; i < sources.length; i++) {
            sources[i] = new RadioButton(energySourcesNames[i]);
            sources[i].setToggleGroup(RB_Group);
        }
        sources[0].setSelected(true);

        VBox result = new VBox(10);
        result.getChildren().add(title);
        result.getChildren().addAll(sensors);
        result.getChildren().add(sourcesTitle);
        result.getChildren().addAll(sources);
        result.setPadding(new Insets(10, 10, 10, 10));
        result.setAlignment(Pos.CENTER_LEFT);
        return result;
    }

    /*
    public void createRobot() {
        robot = new Robot();
    }

    public void createRobot(double rotateDegree, double speed) {
        robot = new Robot(rotateDegree, speed);
    }

    public Robot getRobot() {
        return robot;
    }
    */
}
