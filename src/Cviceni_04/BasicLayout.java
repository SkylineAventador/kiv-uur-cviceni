package Cviceni_04;

import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class BasicLayout extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane mainBP = new BorderPane();
        ServiceBot servant = new ServiceBot(); // Layout service class.

        servant.configBorderPane(mainBP); // Works only with BorderPane (according to task).
        servant.configStage(primaryStage, mainBP);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
