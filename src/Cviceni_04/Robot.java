package Cviceni_04;

enum Status {
    STAND_BY,
    POWER_OFF
}

/**
 * @author Dmytro Kravtsov
 * @deprecated Zacatek napsani tridy byl jeste pred poradnem precteni zadani ulohy =)
 */
public class Robot {
    private double rotateDegree;
    private double speed;
    private Status status;

    public Robot() {
        rotateDegree = 0.0;
        speed = 0.0;
        status = Status.STAND_BY;
    }

    public Robot(double rotateDegree, double speed) {
        setRotateDegree(rotateDegree);
        setSpeed(speed);
        status = Status.STAND_BY;
    }

    public double getRotateDegree() {
        return rotateDegree;
    }

    public double getSpeed() {
        return speed;
    }

    public Status getStatus() {
        return status;
    }

    public void setRotateDegree(double rotateDegree) {
        if (rotateDegree < 0) {
            this.rotateDegree = 0.0;
        } else if (rotateDegree > 360) {
            this.rotateDegree = 360;
        } else
            this.rotateDegree = rotateDegree;
    }

    public void setSpeed(double speed) {
        if (speed < 0) {
            this.speed = 0.0;
        } else if (speed > 7) {
            this.speed = 10;
        } else
            this.speed = speed;
    }

    public void powerOff() {
        setSpeed(0);
        setRotateDegree(0);
        status = Status.POWER_OFF;
    }
}
