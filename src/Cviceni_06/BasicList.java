package Cviceni_06;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class BasicList extends Application {
    //Window dimensions
    private final int WINDOW_WIDTH = 800;
    private final int WINDOW_HEIGHT = 600;
    private final int LIST_LINES_COUNT = 10; //Pocatecny pocet radek v seznamu
    private final int TARGET_DATA_VALUES_COUNT = 5;
    private ListView<String> list_LV;
    private String[][] persons_data;

    private TextField[] tarInfBox_dataFields = new TextField[TARGET_DATA_VALUES_COUNT];

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        configPrimaryStage(primaryStage);
        primaryStage.show();
    }

    /**
     * Metoda nastavi vsechny zakladni vlastnosti hlavni stage
     * @param primaryStage hlavni stage aplikace
     */
    private void configPrimaryStage(Stage primaryStage) {
        primaryStage.setScene(getScene(getPane()));
        primaryStage.setMinWidth(640);
        primaryStage.setMinHeight(480);
        primaryStage.setTitle("Cviceni 06 - List");
    }

    private BorderPane getPane() {
        BorderPane pane = new BorderPane();
        setBorderPaneSides(pane);
        return pane;
    }

    private Scene getScene(BorderPane pane) {
        return new Scene(pane, WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    private void setBorderPaneSides(BorderPane pane) {
        list_LV = new ListView<String>(createAndFillList());
        persons_data = new String[list_LV.getItems().size()][5];
        for (int i = 0; i < persons_data.length; i++) {
            for (int j = 0; j < persons_data[i].length; j++) {
                persons_data[i][j] = "Unknown";
            }
        }
        list_LV.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        list_LV.setOnMouseClicked(event -> loadPersonData(list_LV.getSelectionModel().getSelectedIndex()));
        pane.setCenter(list_LV);
        pane.setBottom(getBottomContent());
    }

    private void loadPersonData(int clickedIndex) {
        if (clickedIndex >= 0) {
            for (int i = 0; i < tarInfBox_dataFields.length; i++) {
                tarInfBox_dataFields[i].setDisable(false);
                tarInfBox_dataFields[i].setText(persons_data[clickedIndex][i]);
            }
        }
    }

    private void savePersonData() {
        int selectedIndex = list_LV.getSelectionModel().getSelectedIndex();
        for (int i = 0; i < persons_data[selectedIndex].length; i++) {
            persons_data[selectedIndex][i] = tarInfBox_dataFields[i].getText();
        }
        list_LV.getItems().set(selectedIndex, (persons_data[selectedIndex][0]
                + " " + persons_data[selectedIndex][1]));

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("System Information");
        alert.setHeaderText("Success");
        alert.setContentText("Data for selected person were successfully saved");
        alert.showAndWait();
    }

    private void printDataModelContent() {
        Alert dataWindow = new Alert(Alert.AlertType.INFORMATION);
        dataWindow.setTitle("Persons data summary");
        dataWindow.setHeaderText("Beside you can see data\nof every person in the data list:");
        StringBuilder builder = new StringBuilder();
        for (String[] person_data : persons_data) {
            builder.append("Name: ").append(person_data[0]).append('\n');
            builder.append("Surname: ").append(person_data[1]).append('\n');
            builder.append("E-Mail: ").append(person_data[2]).append('\n');
            builder.append("City: ").append(person_data[3]).append('\n');
            builder.append("Post Code: ").append(person_data[4]).append('\n');
            builder.append('\n');
        }
        TextArea content_TA = new TextArea(builder.toString());
        dataWindow.setGraphic(content_TA);
        dataWindow.showAndWait();
    }

    private HBox getBottomControls() {
        HBox result = new HBox();

        Button reset_BT = new Button("Reset");
        reset_BT.setOnAction(event -> {list_LV.getSelectionModel().clearSelection();
            for (TextField dataField : tarInfBox_dataFields) {
                dataField.clear();
                dataField.setDisable(true);
            }});

        Button save_BT = new Button("Save");
        save_BT.setOnAction(event -> savePersonData());

        Button print_BT = new Button("Print All");
        print_BT.setOnAction(event -> printDataModelContent());

        result.getChildren().addAll(save_BT, reset_BT, print_BT);
        result.setSpacing(10);
        result.setAlignment(Pos.CENTER);
        result.setPadding(new Insets(10, 5, 10, 5));

        return result;
    }

    private HBox getTargetInfoBox() {
        HBox result = new HBox();

        VBox[] dataCols = new VBox[TARGET_DATA_VALUES_COUNT];
        Label[] labels = new Label[]{
                new Label("Name:"),
                new Label("Surname:"),
                new Label("E-mail:"),
                new Label("City:"),
                new Label("Post Code:")
        };

        for (int i = 0; i < TARGET_DATA_VALUES_COUNT; i++) {
            dataCols[i] = new VBox();
            tarInfBox_dataFields[i] = new TextField();
            tarInfBox_dataFields[i].setDisable(true);

            dataCols[i].getChildren().addAll(labels[i], tarInfBox_dataFields[i]);
            dataCols[i].setSpacing(5);
        }

        result.getChildren().addAll(dataCols);
        result.setSpacing(10);
        result.setPadding(new Insets(5, 2, 5, 2));
        return result;
    }

    private VBox getBottomContent() {
        VBox result = new VBox();
        result.setPadding(new Insets(10, 5, 10, 5));

        result.getChildren().addAll(getTargetInfoBox(), getBottomControls());

        return result;
    }

    private ObservableList<String> createAndFillList() {
        ObservableList<String> list = FXCollections.observableArrayList();
        for (int i = 0; i < LIST_LINES_COUNT; i++) {
            list.add("Man " + (i + 1));
        }
        return list;
    }
}
