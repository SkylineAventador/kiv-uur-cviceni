package Cviceni_07;

public enum FontEnum {
    REGULAR,BOLD,ITALIC;

    @Override
    public String toString() {
        switch (this){
            case BOLD:{
                return "Tučný";
            }
            case ITALIC:{
                return "Kurzíva";
            }
            case REGULAR:{
                return "Normální";
            }
            default:
                return "Neznamý";
        }
    }
}
