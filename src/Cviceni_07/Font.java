package Cviceni_07;

import javafx.scene.paint.Color;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class Font {
    private String name;
    private Color color;
    private FontEnum rez;
    private int size;
    private boolean visibility;
    private Text result;

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public FontEnum getRez() {
        return rez;
    }

    public int getSize() {
        return size;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public Font(String name, Color color, FontEnum rez, int size, boolean visibility) {
        this.name = name;
        this.color = color;
        this.rez = rez;
        this.size = size;
        this.visibility = visibility;
        this.result = new Text(name);
        this.result.setFontSmoothingType(FontSmoothingType.LCD);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRez(FontEnum rez) {
        this.rez = rez;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public Text getResult() {
        result.setText(name);
        if (rez.equals(FontEnum.BOLD)) {
            result.setFont(javafx.scene.text.Font.font(name, FontWeight.BOLD, size));
        } else {
            result.setFont(javafx.scene.text.Font.font(name, FontPosture.findByName(rez.name()), size));
        }
        result.setFill(color);
        result.setVisible(visibility);
        return result;
    }

    public Font() {
        this.name = "Arial";
        this.color = Color.BLACK;
        this.rez = FontEnum.REGULAR;
        this.size = 12;
        this.visibility = true;
        this.result = new Text(this.name);
        this.result.setFontSmoothingType(FontSmoothingType.LCD);
    }

    @Override
    public String toString() {
        return name;
    }
}
