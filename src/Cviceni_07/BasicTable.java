package Cviceni_07;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

import java.util.Locale;

// TODO: 10. 6. 2019 Implement table controls and work we'll be done.
public class BasicTable extends Application {
    public static final String DEF_LANGUAGE = "cs";
    public static final String DEF_COUNTRY = "CZ";

    // reference on stage
    private Stage primaryStage;
    // reference on table, neccessary to manipulate with selected elements
    private TableView<Font> tableTV;
    // references on textfields and date picker for adding new people
//    private TextField nameTF;
//    private ColorPicker colorCP;
//    private ComboBox<FontEnum> rezDP;
//    private DoubleProperty sizeSDP;
//    private ComboBox<StringProperty> visibilityCB;
//    private StringProperty resultFontSSP;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));

        this.primaryStage.setTitle("Table example");
        this.primaryStage.setScene(createScene());
        this.primaryStage.show();
    }

    private Scene createScene() {
        Scene scene = new Scene(getRoot(), 800, 600);
        return scene;
    }

    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();

        rootPane.setCenter(getTablePane());
        rootPane.setBottom(getControlPane());

        return rootPane;
    }

    private Node getTablePane() {
        tableTV = new TableView<Font>();

        TableColumn<Font, String> nameColumn = new TableColumn<Font, String>("Name");
        TableColumn<Font, Color> colorColumn = new TableColumn<Font, Color>("Color");
        TableColumn<Font, FontEnum> rezColumn = new TableColumn<Font, FontEnum>("Rez");
        TableColumn<Font, Integer> sizeColumn = new TableColumn<Font, Integer>("Size");
        TableColumn<Font, Boolean> visibilityColumn = new TableColumn<Font, Boolean>("Visibility");
        TableColumn<Font, Text> resultColumn = new TableColumn<Font, Text>("Example");



        nameColumn.setCellValueFactory(new PropertyValueFactory<Font, String>("name"));
        nameColumn.setMinWidth(130);
        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(event -> {
            event.getRowValue().setName(event.getNewValue());
            tableTV.refresh();
        });

        colorColumn.setCellValueFactory(new PropertyValueFactory<Font, Color>("color"));
        colorColumn.setMinWidth(130);
        colorColumn.setCellFactory(column -> new ColorPickerCell());
        colorColumn.setOnEditCommit(this::commitColor);

        rezColumn.setCellValueFactory(new PropertyValueFactory<Font, FontEnum>("rez"));
        rezColumn.setMinWidth(130);
        rezColumn.setCellFactory(ComboBoxTableCell.forTableColumn(FontEnum.values()));
        rezColumn.setOnEditCommit(event -> {
            event.getRowValue().setRez(event.getNewValue());
            tableTV.refresh();
        });

        sizeColumn.setCellValueFactory(new PropertyValueFactory<Font, Integer>("size"));
        sizeColumn.setMinWidth(130);
        sizeColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        try {
            sizeColumn.setOnEditCommit(this::commitSize);
        } catch (IllegalArgumentException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Chyba");
            alert.setHeaderText("Chyba ulozeni hodnoty");
            alert.setContentText("Zadana hodnota neni cislem nebo je mensi nez 0. Platny rozsah je 1 az 24. Opakujte akci prosim.");
            alert.showAndWait();
        }


        visibilityColumn.setCellValueFactory(new PropertyValueFactory<Font, Boolean>("visibility"));
        visibilityColumn.setMinWidth(130);
        visibilityColumn.setCellFactory(ComboBoxTableCell.forTableColumn(Boolean.TRUE, Boolean.FALSE));
        visibilityColumn.setOnEditCommit(event -> {
            event.getRowValue().setVisibility(event.getNewValue());
            tableTV.refresh();
        });

        resultColumn.setCellValueFactory(new PropertyValueFactory<Font, Text>("result"));
        resultColumn.setMinWidth(130);
        resultColumn.setEditable(false);


        // adding columns to the table
        tableTV.getColumns().addAll(nameColumn, colorColumn, rezColumn, sizeColumn, visibilityColumn, resultColumn);
        // creating initial dataset
        tableTV.setItems(getInitData());
        // allowing editation of the table and all its columns
        tableTV.setEditable(true);
        // allowing selection of multiple rows
        tableTV.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // allowing columns to resize in order to fill the whole table
        tableTV.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // adding reaction on key pressed in the table - deleting rows when DELETE is pressed
        tableTV.setOnKeyReleased(event -> {
            // deleting only when DELETE is pressed, other keys are not interesting
            if (event.getCode() == KeyCode.DELETE)
                deleteSelected();
        });

        BorderPane.setMargin(tableTV, new Insets(5));

        return tableTV;
    }

    private Node getControlPane() {
        HBox result = new HBox(10);
        result.setPadding(new Insets(10));
        result.setAlignment(Pos.CENTER);

        Button addDefault = new Button("Insert");
        addDefault.setOnAction(event -> insertDefaultRecord());

        Button delete = new Button("Delete");
        delete.setOnAction(event -> deleteSelected());

        result.getChildren().addAll(addDefault, delete);
        return result;
    }

    // creating initial data
    private ObservableList<Font> getInitData() {
        ObservableList<Font> data = FXCollections.observableArrayList();

        data.add(new Font("Calibri", Color.RED, FontEnum.ITALIC, 18, true));
        data.add(new Font());

        return data;
    }


    private void commitColor(TableColumn.CellEditEvent<Font, Color> event) {
        event.getRowValue().setColor(event.getNewValue());
        tableTV.refresh();
    }

    private void commitSize(TableColumn.CellEditEvent<Font, Integer> event) throws IllegalArgumentException {
        if (event.getNewValue() == (int) event.getNewValue() && event.getNewValue() > 0) {
            event.getRowValue().setSize(event.getNewValue());
        } else
            throw new IllegalArgumentException();
        tableTV.refresh();
    }

    // deleting selected elements, similar like in ListView
    private void deleteSelected() {
        // obtaining the selected elements
        ObservableList<Font> selection = FXCollections.observableArrayList(tableTV.getSelectionModel().getSelectedItems());

        // when nothing is selected
        if (selection.size() == 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Deleting items");
            alert.setHeaderText("No items for deleting were selected!");
            alert.setContentText("Please select items for deleting.");
            alert.show();
        } else {
            tableTV.getItems().removeAll(selection);
            tableTV.getSelectionModel().clearSelection();
        }
    }

    private void insertDefaultRecord() {
        tableTV.getItems().add(new Font());
    }

}
