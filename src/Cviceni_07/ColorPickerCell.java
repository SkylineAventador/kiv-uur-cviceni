package Cviceni_07;

import javafx.geometry.Insets;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class ColorPickerCell extends TableCell<Font, Color> {
    private ColorPicker colorCP;


    // switching to the editation mode
    public void startEdit() {
        // propagation of mode switching to the parent class
        super.startEdit();

        // creating editor - DatePicker if it is not already prepared
        if (colorCP == null) {
            createColorPicker();
        }
        // disabling label with text
        setText(null);
        // setting up editor
        setGraphic(colorCP);

        // opening editor (fields with calendar)
        colorCP.show();
    }

    // switching from editation to presentation mode
    public void cancelEdit() {
        super.cancelEdit();

        System.out.println("canceling editation");

        // returns value in datepicker to original value
        colorCP.setValue(getItem());

        // setting label
        //setText(colorCP.getValue().toString());
        // removing date picker
        setGraphic(null);
    }

    // creating date picker
    private void createColorPicker() {
        colorCP = new ColorPicker(getItem());

        // consuming DELETE key event so it is not invoking row deletion when datePicker is open
        colorCP.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.DELETE) {
                event.consume();
            }
        });

        // reaction on commit - when date is selected, date picker have to create commit event
        colorCP.setOnAction(event -> {
            // commit when date was selected


            if (colorCP.getValue() != null) {
                commitEdit(colorCP.getValue());
                // cancel when no date was provided
            } else {
                event.consume();
                cancelEdit();
            }
        });
    }

    // setting value to the cell when model is changed or manipulated
    // this method have to be always overloaded, when new cell type is created
    public void updateItem(Color item, boolean empty) {
        // propagating change to the parent class
        super.updateItem(item, empty);

        // setting font for the label
        setFont(new javafx.scene.text.Font("Arial", 15));
        // when no item is provided, cell is showing no information
        if (empty) {
            setText(null);
            setGraphic(null);
            this.setBackground(null);
        } else {
            // in editation mode
            if (isEditing()) {
                // setting the value to editor - color picker
                if (colorCP != null) {
                    colorCP.setValue(item);
                }
                // disabling label
                setText(null);
                // setting up editor
                setGraphic(colorCP);
                // in presentation mode
            } else {
                // setting the date to label
                //setText(item.toString());
                this.setBackground(new Background(new BackgroundFill(item, CornerRadii.EMPTY, Insets.EMPTY)));
                // disabling editor
                setGraphic(null);
            }
        }
    }
}
